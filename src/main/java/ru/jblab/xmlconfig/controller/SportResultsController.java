package ru.jblab.xmlconfig.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.jblab.xmlconfig.controller.validator.KindOfSportValidator;
import ru.jblab.xmlconfig.form.KindOfSportForm;
import ru.jblab.xmlconfig.model.KindOfSport;
import ru.jblab.xmlconfig.model.enums.KindOfSportEnum;
import ru.jblab.xmlconfig.service.ISportResultService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Arcoo on 07.10.2014.aa
 */

@Controller
public class SportResultsController {

    @Autowired
    @Qualifier("WithDBSportRes")
    ISportResultService sportResService;

    @Autowired
    private KindOfSportValidator validator;

    @RequestMapping(value = "/sportResults/sportRes", method = RequestMethod.GET)
    public String renderSportResPage(ModelMap map) {
        map.addAttribute("kindOfSport", new KindOfSportForm());
        //map.addAttribute("listOfSports", sportResService.getKindsList());       // !!!!!!!!!!!
//        Map<String, List<Event>> mapEvent = timeTableEventService.getAllEvents();
//        map.addAttribute("events", mapEvent);
//        map.addAttribute("eventEnum", TimeTableEnum.getAll());
//        map.addAttribute("mapSportResults", service.getAllSportResults());
        map.addAttribute("sportKindsEnumList", KindOfSportEnum.values());   //returns List of enums;
//        map.addAttribute("defaultSportKindsEnum", KindOfSportEnum.getDefault());
        return "sportResults/sportRes";
    }

    @RequestMapping(value = "/sportResults/addSportRes", method = RequestMethod.POST)
    public String addKindOfsport(@ModelAttribute("kindOfSport") KindOfSportForm kindOfSportForm,
                                 BindingResult bindingResult, ModelMap map) throws ParseException {
        validator.validate(kindOfSportForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "sportResults/sportRes";
        } else {
            sportResService.addSportRes(kindOfSportForm);
            return "redirect:/sportResults/sportResDiary";
        }
    }

    @RequestMapping(value = "/sportResults/edit/{id}", method = RequestMethod.GET)
    public String editKindOfSport(@PathVariable("id") Long id, ModelMap map) {
        map.addAttribute("kindOfSportToEdit", FormMapper.kindOfSportToKindOfSportForm(sportResService.searchKindOfSportById(id)));
        map.addAttribute("sportKindsEnumList", KindOfSportEnum.values());
        return "/sportResults/editSportRes";
    }

    @RequestMapping(value = "/sportResults/editSportRes/{id}", method = RequestMethod.POST)
    public String updateKindOfSport(@ModelAttribute("kindOfSportToEdit") KindOfSportForm kindOfSportForm,
                                    @PathVariable("id") Long id, BindingResult bindingResult,
                                    ModelMap map) throws ParseException {
        validator.validate(kindOfSportForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "/sportResults/editSportRes";
        } else {
            kindOfSportForm.setId(id);
            sportResService.editSportRes(kindOfSportForm);      // replaces old element to new edited element in the List;
            return "redirect:/sportResults/sportResDiary";     //"redirect:/sportResults/sportRes";
        }
    }

    @RequestMapping(value = "/sportResults/delete/{id}", method = RequestMethod.GET)
    public String deleteKindOfSport(@PathVariable("id") Long id, ModelMap map) {
        sportResService.deleteKindOfSport(id);             // deletes an object, which was founded by it's attribute;
        return "redirect:/sportResults/sportRes";          // and pushes changed List<KindOfSports> into session;
    }

    @RequestMapping(value = "/sportResults/clearSportRes", method = RequestMethod.GET)
    public String clearListOfSportResults() {
        sportResService.clearListOfSportResults();
        return "redirect:/sportResults/sportRes";
    }

    @RequestMapping(value = "/sportResults/clearSportResAjax", method = RequestMethod.POST)
    public String clearListOfSportResultsAjax(@RequestParam("date") String date, ModelMap map) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Long dateLong = sdf.parse(date).getTime();
        sportResService.deleteSpecificListByDate(dateLong);
        List<KindOfSport> specificListOfSports = sportResService.findSportResultsByDate(dateLong);
        map.addAttribute("specificListOfSports", specificListOfSports);

        return "/sportResults/addSpecSportResListInclude";  //redirect:/
    }

    @RequestMapping(value = "/sportResults/sportResDiary", method = RequestMethod.GET)
    public String renderSportResDiaryPage() {
        return "sportResults/sportResDiary";
    }

    // for Ajax request method edit
    @RequestMapping(value = "/sportResults/editAjax", method = RequestMethod.POST)
    @ResponseBody
    public KindOfSport editKindOfSportAjax(@ModelAttribute("kindOfSportToEdit") KindOfSportForm kindOfSportForm)
            throws ParseException {
        sportResService.editSportRes(kindOfSportForm);
        KindOfSport kindOfSport = (FormMapper.kindOfSportFormToKindOfSport(kindOfSportForm));
        return kindOfSport;
    }

    // for Ajax request method add
    @RequestMapping(value = "/sportResults/addAjax", method = RequestMethod.POST)
    //@ResponseBody
    public String addKindOfSportAjax(@ModelAttribute("kindOfSport") KindOfSportForm kindOfSportForm,
                                     ModelMap map) throws ParseException {
        KindOfSport kindOfSport = sportResService.addSportRes(kindOfSportForm);
        map.addAttribute("kindOfsport", kindOfSport);
        return "/sportResults/addsportresInclude";
    }

    @RequestMapping(value = "/sportResults/findByDateAjax", method = RequestMethod.GET)
    //@ResponseBody
    public String findNote(@RequestParam("date") String date, ModelMap map)
            throws ParseException, UnsupportedEncodingException, JsonProcessingException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Long dateLong = sdf.parse(date).getTime();
        List<KindOfSport> specificListOfSports = sportResService.findSportResultsByDate(dateLong);
        map.addAttribute("specificListOfSports", specificListOfSports);

        return "/sportResults/addSpecSportResListInclude";
    }

    @RequestMapping(value = "/sportResults/deleteSportResAjax", method = RequestMethod.GET)
    public String deleteKindOfSportAjax(@RequestParam("id") Long id, ModelMap map) {
        KindOfSport kindOfSport = sportResService.searchKindOfSportById(id);
        Long dateLong = kindOfSport.getDate();
        sportResService.deleteKindOfSport(id);             // deletes an object, which was founded by it's attribute;

        List<KindOfSport> specificListOfSports = sportResService.findSportResultsByDate(dateLong);
        map.addAttribute("specificListOfSports", specificListOfSports);

        return "/sportResults/addSpecSportResListInclude";
    }


}