package ru.jblab.xmlconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.jblab.xmlconfig.model.HelloModel;
import ru.jblab.xmlconfig.service.HelloService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ainurminibaev on 07.10.14.
 */
@Controller
public class HelloController {

    @Autowired
    HttpSession session;

    @Autowired
    HelloService helloService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String renderHelloPage(ModelMap map) {
        List<HelloModel> helloModels = helloService.findAll();
        map.addAttribute("name", "Ainur");
        map.addAttribute("resultList", session.getAttribute("resultsList"));
        map.addAttribute("helloList", helloModels);
        return "hello";
    }

    @RequestMapping(value = "/sum", method = RequestMethod.GET)
    public String sum(String a, String b, ModelMap map) {
        if (a == null || b == null) {
            return "redirect:/";
        }
        Long aLong = Long.valueOf(a);
        Long bLong = Long.valueOf(b);
        long result = aLong + bLong;
        map.addAttribute("result", result);
        List<Long> resultsList = (List<Long>) session.getAttribute("resultsList");
        if (resultsList != null) {
            resultsList.add(result);
        } else {
            resultsList = new ArrayList<>();
            resultsList.add(result);
        }
        session.setAttribute("resultsList", resultsList);
        return "sumPage";
    }

    @RequestMapping(value = "/hello/add", method = RequestMethod.POST)
    public String addHelloModel(@RequestParam String name, ModelMap map) {
        HelloModel helloModel = new HelloModel();
        helloModel.setName(name);
        helloService.save(helloModel);
        return "redirect:/";
    }


    @RequestMapping(value = "/sumAjax", method = RequestMethod.POST)
    @ResponseBody
    public HelloModel sum(@RequestParam Long a,
                          @RequestParam Long b,
                          @RequestParam(required = false, defaultValue = "anonymous") String name) {
        long result = a + b;
        HelloModel helloModel = new HelloModel();
        helloModel.setName(name);
        helloModel.setResult(String.valueOf(result));
        return helloModel;
    }
}
