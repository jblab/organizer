package ru.jblab.xmlconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.jblab.xmlconfig.controller.validator.TimeTableFormValidator;
import ru.jblab.xmlconfig.form.EventForm;
import ru.jblab.xmlconfig.model.Event;
import ru.jblab.xmlconfig.model.enums.TimeTableEnum;
import ru.jblab.xmlconfig.service.TimeTableService;
import ru.jblab.xmlconfig.utils.FormMapper;


import java.util.List;
import java.util.Map;

/**
 * Created by Dilya on 07.10.2014.
 */
@Controller
public class TimeTableController {

    @Qualifier("DB")
    @Autowired
    TimeTableService timeTableEventService;

    private TimeTableFormValidator validator;

    @Autowired
    public TimeTableController(TimeTableFormValidator validator) {
        this.validator = validator;
    }

    @RequestMapping(value = "/timetable/create", method = RequestMethod.GET)
    public String renderForm(ModelMap map) {
        map.addAttribute("event", new EventForm());
        map.addAttribute("enum", TimeTableEnum.values());
        map.addAttribute("myDefault", TimeTableEnum.getDefault().getValue());
        return "timetable/create";
    }

    @RequestMapping(value = "/timetable/add", method = RequestMethod.POST)
    //public String addEvent(@RequestParam("EventName") String name, @RequestParam("weekday") String weekday, @RequestParam("time") String time, ModelMap map) {
    public String addEvent(@ModelAttribute("event") EventForm eventForm, BindingResult bindingResult, ModelMap model) {
        validator.validate(eventForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "timetable/create";
        } else {
            timeTableEventService.addEvent(eventForm);
            return "redirect:/timetable/show";
        }
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteEvent(@PathVariable("id") Long id, ModelMap map) {
        timeTableEventService.deleteEvent(id);
        return "redirect:/timetable/show";
    }

    @RequestMapping(value = "/timetable/edit/{id}", method = RequestMethod.GET)
    public String editEvent(@PathVariable("id") Long id, ModelMap map) {
        map.addAttribute("eventForm", timeTableEventService.searchEvent(id));
        return "/timetable/edit";
    }

    @RequestMapping(value = "/timetable/edit/{id}", method = RequestMethod.POST)
    public String editEvent(@ModelAttribute("eventForm") EventForm eventForm, @PathVariable("id") Long id, BindingResult bindingResult, ModelMap map) {
        validator.validate(eventForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "timetable/edit";
        } else {
            eventForm.setId(id);
            timeTableEventService.edit(eventForm);
            return "redirect:/timetable/show";
        }
    }

    @RequestMapping(value = "/timetable/addAjax", method = RequestMethod.POST)
    @ResponseBody
    public Event addEvent(@ModelAttribute("event") EventForm eventForm ) {
        timeTableEventService.addEvent(eventForm);
        Event event = (FormMapper.eventFormToEvent(eventForm));
        return event;
    }

    @RequestMapping(value = "/timetable/show", method = RequestMethod.GET)
    public String renderEventsList(ModelMap map) {
        List<Event> list= timeTableEventService.getEventsByDay("Monday");
        map.addAttribute("events", list);
        map.addAttribute("eventEnum", TimeTableEnum.getAll());

        return "timetable/show";
    }
    @RequestMapping(value = "/timetable/showAjax", method = RequestMethod.POST)
    public String showEvents(@RequestParam("weekday") String weekday, ModelMap map) {
        map.addAttribute("events", timeTableEventService.getEventsByDay(weekday));

        return "timetable/include";
    }
}
