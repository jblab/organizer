package ru.jblab.xmlconfig.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.jblab.xmlconfig.controller.validator.NoteFormValidator;
import ru.jblab.xmlconfig.form.NoteForm;
import ru.jblab.xmlconfig.model.NoteModel;
import ru.jblab.xmlconfig.model.User;
import ru.jblab.xmlconfig.service.DiaryService;
import ru.jblab.xmlconfig.utils.AuthenticationUtil;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Rigen on 07.10.14.
 */
@Controller
public class DiaryController {

    @Autowired
    @Qualifier("NoteService")
    DiaryService diaryService;

    @RequestMapping(value = "/diary", method = RequestMethod.GET)
    public String renderDiaryPage(ModelMap map) {
        map.addAttribute("note", new NoteForm());
        map.addAttribute("notesList", diaryService.getNotesFormList());
        return "/diary/diary";
    }

    @RequestMapping(value = "/diary/add", method = RequestMethod.POST)
    public String addNote(@ModelAttribute("note") NoteForm note, BindingResult bindingResult, ModelMap map) throws ParseException {
        map.addAttribute("notesList", diaryService.getNotesFormList());
        bindingResult = NoteFormValidator.validate(note, bindingResult);
        if (bindingResult.hasErrors()) {
            return "/diary/diary";
        } else {
            diaryService.addNote(FormMapper.noteFormToNote(note));
            return "redirect:/diary";
        }

    }

    @RequestMapping(value = "/diary/addOrEdit", method = RequestMethod.POST)
    public String findNote(@ModelAttribute("note") NoteForm note, ModelMap map) throws ParseException {
        note.setUser(AuthenticationUtil.getAuthUser());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Long dateLong = sdf.parse(note.getDate()).getTime();
        NoteModel noteModel = diaryService.findNoteByPK(dateLong, note.getUser());
        if (noteModel == null) {
            if (!note.getNoteText().isEmpty())
                diaryService.addNote(FormMapper.noteFormToNote(note));
        } else {
            if (!note.getNoteText().isEmpty()) {
                noteModel.setNoteText(note.getNoteText());
                diaryService.editNote(noteModel);
            } else {
                diaryService.deleteNote(noteModel.getNoteKey());
            }
        }
        return null;
    }

    @RequestMapping(value = "/diary/findByDate", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> findNote(@RequestParam("date") String date) throws ParseException, UnsupportedEncodingException, JsonProcessingException {
        User currentUser = AuthenticationUtil.getAuthUser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Long dateLong = sdf.parse(date).getTime();
        NoteModel noteModel = diaryService.findNoteByPK(dateLong, currentUser);
        if (noteModel == null) {
            return null;
        } else {
            return new ResponseEntity<>(noteModel.getNoteText(), HttpStatus.OK);
        }
    }

}
