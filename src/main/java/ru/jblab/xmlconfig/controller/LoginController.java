package ru.jblab.xmlconfig.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.jblab.xmlconfig.model.User;
import ru.jblab.xmlconfig.utils.AuthenticationUtil;

/**
 * Created by Александр on 05.11.14.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(
            @RequestParam(value = "error", required = false) String error,
            ModelMap map) {
        if (error != null) {
            map.addAttribute("error", "Invalid username and password!");
        }
        User user = AuthenticationUtil.getAuthUser();
        if(user != null)
            return "redirect:/";
        else
            return "login";
    }
}
