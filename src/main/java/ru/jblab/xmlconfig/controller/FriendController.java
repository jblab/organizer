package ru.jblab.xmlconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.jblab.xmlconfig.controller.validator.FriendValidator;
import ru.jblab.xmlconfig.form.FriendForm;
import ru.jblab.xmlconfig.model.enums.FriendTypeEnum;
import ru.jblab.xmlconfig.service.FriendService;


/**
 * Created by Александр on 07.10.14.
 */
@Controller
public class FriendController {

    @Autowired
    @Qualifier("friend")
    FriendService friendService;

    @RequestMapping(value = "/friend", method = RequestMethod.GET)
    public String friendGet(ModelMap map) {
//        map.addAttribute("error", "");
        map.addAttribute("friendForm", new FriendForm());
//        Map<String, List<Friend>> mapFriend = friendService.getAllFriend();
        map.addAttribute("friendEnum", FriendTypeEnum.getAll());
//        map.addAttribute("friendMap", mapFriend);
        return "friend/friend";
    }

//    @RequestMapping(value = "/setTypeFriend*", method = RequestMethod.POST)
//    public String setTypeUser(ModelMap map, @RequestParam("id") String id, @RequestParam("menu") String type) {
//        friendService.setTypeFriend(id, type);
//        return "redirect:/friend";
//    }
//
//    @RequestMapping(value = "/deleteFriend*", method = RequestMethod.POST)
//    public String deleteFriend(@RequestParam("id") String id, ModelMap map) {
//        friendService.deleteFriend(id);
//        return "redirect:/friend";
//    }

    @RequestMapping(value = "/add/friend", method = RequestMethod.POST)
    public String addFriendOnDB(@ModelAttribute("friendForm") FriendForm friendForm,
                                BindingResult result, ModelMap map) {
        result = FriendValidator.validForm(friendForm, result);
        if (result.hasErrors()) {
            map.addAttribute("friendEnum", FriendTypeEnum.getAll());
            return "friend/friend";
        } else {
            friendService.addFriend(friendForm);
            return "redirect:/friend";
        }
    }

}
