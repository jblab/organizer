package ru.jblab.xmlconfig.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.jblab.xmlconfig.form.TaskForm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ToDoFormValidator implements Validator {


    @Override
    public boolean supports(Class<?> clazz) {
        return TaskForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "taskname", "todolist.emptyName");
        TaskForm taskForm = (TaskForm) target;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = null;
        try {
            if (!taskForm.getDeadlineDate().equals("") && !taskForm.getDeadlineTime().equals("")) {
                date = sdf.parse(taskForm.getDeadlineDate() + " " + taskForm.getDeadlineTime());
            } else if (!taskForm.getDeadlineDate().equals("")) {
                date = sdf.parse(taskForm.getDeadlineDate() + " 12:00");
            }
        } catch (ParseException e) {
            errors.rejectValue("deadlineDate", "todolist.wrongData");
        }
        if (date != null && date.before(new Date())) {
            errors.rejectValue("deadlineDate", "todolist.earlyData");
        }
    }
}
