package ru.jblab.xmlconfig.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.jblab.xmlconfig.model.KindOfSport;


/**
 * Created by Arcoo on 09.10.2014.
 */
@Component
public class KindOfSportValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return KindOfSport.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "sportsError.emptyField");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "time", "sportsError.emptyField");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "metric", "sportsError.emptyField");
    }

    /*public void validateWithoutName(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "time", "sportsError.emptyField");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "metric", "sportsError.emptyField");
    }*/
}

