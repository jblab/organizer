package ru.jblab.xmlconfig.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.jblab.xmlconfig.model.Goods;

@Component
public class FormShopValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Goods.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nameOfProduct", "shop.nameisempty");
    }
}
