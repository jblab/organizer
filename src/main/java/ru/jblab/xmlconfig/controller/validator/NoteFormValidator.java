package ru.jblab.xmlconfig.controller.validator;

import org.springframework.validation.BindingResult;
import ru.jblab.xmlconfig.form.NoteForm;

/**
 * Created by Rigen on 08.10.14.
 */

//@Component("noteFormValidator") implements Validator
public class NoteFormValidator {

    public static BindingResult validate(NoteForm noteForm, BindingResult bindingResult) {
        if (noteForm.getDate().equals("")) {
            bindingResult.rejectValue("date", "NotEmpty", "");
        }
        if (noteForm.getNoteText().equals(""))
            bindingResult.rejectValue("noteText", "NotEmpty", "");
        return bindingResult;
    }


//    @Override
//    public boolean supports(Class<?> clazz) {
//        return NoteForm.class.isAssignableFrom(clazz);
//    }
//
//    @Override
//    public void validate(Object target, Errors errors) {
//        NoteForm noteForm = (NoteForm) target;
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "NotEmpty");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noteText", "NotEmpty");
//    }
}
