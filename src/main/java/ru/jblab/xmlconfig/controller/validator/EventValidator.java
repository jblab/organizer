package ru.jblab.xmlconfig.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.jblab.xmlconfig.form.EvForm;
import ru.jblab.xmlconfig.model.Ev;

import java.util.regex.Pattern;

/**
 * Created by tommoto on 09.10.14.
 */
@Component
public class EventValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Ev.class.equals(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "ev.field.necessary");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "ev.field.necessary");

        EvForm evfObj = (EvForm) obj;
        if (evfObj.getPrice() != ""){
//            Pattern single = Pattern.compile("/[1-9]+/");
            Pattern scope = Pattern.compile("[1-9]+ - [1-9]+");
            String s1 = new String("[1-9]+");
            String s2 = new String("[1-9]+ {1}-{1} {1}[1-9]+");
            if (!evfObj.getPrice().matches(s1) && !evfObj.getPrice().matches(s2)){
                errors.rejectValue("price", "ev.price");
            }
        }
    }
}
