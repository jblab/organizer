package ru.jblab.xmlconfig.controller.validator;

import org.springframework.validation.BindingResult;
import ru.jblab.xmlconfig.form.FriendForm;

/**
 * Created by Александр on 10.10.14.
 */
public class FriendValidator {
    public static BindingResult validForm(FriendForm friendForm, BindingResult result) {
        if (friendForm.getName().equals("")) {
            result.rejectValue("name", "", "this name is not valid, I so sorry :(");
        }
        return result;
    }
}
