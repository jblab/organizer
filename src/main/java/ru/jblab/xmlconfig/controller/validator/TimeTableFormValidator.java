package ru.jblab.xmlconfig.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.jblab.xmlconfig.form.EventForm;

/**
 * Created by Dilya on 09.10.2014.
 */
@Component
public class TimeTableFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return EventForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eventName", "timetable.emptyname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "time", "timetable.emptyname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "weekday", "timetable.radio");

    }
   /* public void validate(Event event, BindingResult bindingResult) {

        if (event.getEventName().isEmpty())
            bindingResult.addError(new ObjectError("eventName", "Обязательное поле"));
        if (event.getTime().isEmpty())
            bindingResult.addError(new ObjectError("time", "Обязательное поле"));
        if (event.getWeekday().isEmpty())
            bindingResult.addError(new ObjectError("weekday", "Обязательное поле"));
    }*/
}