package ru.jblab.xmlconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.jblab.xmlconfig.controller.validator.EventValidator;
import ru.jblab.xmlconfig.form.EvForm;
import ru.jblab.xmlconfig.model.Ev;
import ru.jblab.xmlconfig.model.enums.EventCategory;
import ru.jblab.xmlconfig.service.EvService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.text.ParseException;

@RequestMapping(value = "/events")
@Controller
public class EvController {

    @Autowired
    private EventValidator validator;

    @Autowired
    @Qualifier("httpEv")
    EvService eventService;

    @RequestMapping(value = "", method = {RequestMethod.POST, RequestMethod.GET})
    public String renderEvents(ModelMap map,
                               @RequestParam(value = "page", required = false, defaultValue = "1") Integer pageNum) {
        map.addAttribute("eventEnums", EventCategory.getValues());
        /*for (int i = 0; i < 60; i++) {
            eventService.addEvent(new Ev("name" + i, (long) 132321123, null, "Cinema"));
        }*/

        int maxPage = (int) (eventService.count() / 10);
        int startPage = (pageNum - 5 >= 1) ? (pageNum - 5) : 1;
        int endPage = (startPage + 10 > maxPage) ? maxPage : startPage + 10;
        map.addAttribute("startPage", startPage);
        map.addAttribute("endPage", endPage);
        map.addAttribute("pageNum", pageNum);

        map.addAttribute("eventsList", eventService.getEventsAtPage(pageNum, null));
        return "events/events";
    }

    @RequestMapping(value = "/cat/{category}", method = {RequestMethod.POST, RequestMethod.GET})
    public String renderCat(ModelMap map,
                            @PathVariable("category") String categoryValue) {
        /*int maxPage = (int) (eventService.count() / 10);
        int startPage = (pageNum - 5 >= 1) ? (pageNum - 5) : 1;
        int endPage = (startPage + 10 > maxPage) ? maxPage : startPage + 10;
        map.addAttribute("startPage", startPage);
        map.addAttribute("endPage", endPage);
        map.addAttribute("pageNum", pageNum);*/

        map.addAttribute("startPage", 0);
        map.addAttribute("pageNum", 0);
        map.addAttribute("endPage", 0);
        map.addAttribute("eventEnums", EventCategory.getValues());
        map.addAttribute("eventsList", eventService.getEventsAtPage(0, EventCategory.fromString(categoryValue)));
        return "events/events";
    }

    @RequestMapping(value = "/event{eventId:[0-9]+}", method = RequestMethod.GET)
    public String showEvent(@PathVariable("eventId") Long eventId,
                            ModelMap map) {
        Ev event = eventService.getEventAt(eventId);
        map.addAttribute("eventEnums", EventCategory.getValues());
        map.addAttribute("categories", EventCategory.values());
        map.addAttribute("event", event);
        return "events/event";
    }

    @RequestMapping(value = "/newEvent", method = RequestMethod.GET)
    public String eventNew(ModelMap map, EvForm evForm) {
        map.addAttribute("eventForm", evForm);
        map.addAttribute("categories", EventCategory.values());
        map.addAttribute("eventEnums", EventCategory.getValues());
        map.addAttribute(evForm);
        return "events/event_edit";
    }

    @RequestMapping(value = "/editEvent", method = RequestMethod.GET)
    public String editEvent(@RequestParam("id") Long id, ModelMap map,
                            EvForm evForm) {
        if (evForm == null) {
            Ev ev = eventService.getEventAt(id);
            evForm = FormMapper.evToEvForm(ev);
        }
        map.addAttribute("eventForm", evForm);
        map.addAttribute("id", id);
        map.addAttribute("categories", EventCategory.values());
        map.addAttribute("eventEnums", EventCategory.getValues());
        return "events/event_edit";
    }

    @RequestMapping(value = "/submitEdit*", method = {RequestMethod.POST, RequestMethod.GET})
    public String submitEdit(@ModelAttribute("eventForm") EvForm evForm,
                         @RequestParam(value = "id") Long id,
                         ModelMap map,
                         BindingResult bindingResult) throws ParseException {
        validator.validate(evForm, bindingResult);
        if (bindingResult.hasErrors()) {
            map.addAttribute("eventForm", evForm);
            map.addAttribute("categories", EventCategory.values());
            map.addAttribute("eventEnums", EventCategory.getValues());
            if (id == null){
                return eventNew(map, evForm);
            } else{
                map.addAttribute("id", id);
                return editEvent(id, map, evForm);
            }
        }
        Ev event = FormMapper.evFormToEv(evForm);
        if (id == null) {
            eventService.addEvent(event);
        } else {
            eventService.update(event);
        }
        return "redirect:/events";
    }

    @RequestMapping(value = "/deleteEvent", method = RequestMethod.POST)
    public String deleteEvent(@RequestParam("id") Long id, ModelMap map) {
        eventService.deleteEventAt(id);
        return "redirect:/events";
    }

}