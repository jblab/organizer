package ru.jblab.xmlconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.jblab.xmlconfig.controller.validator.ToDoFormValidator;
import ru.jblab.xmlconfig.form.TaskForm;
import ru.jblab.xmlconfig.model.enums.TaskPriorityEnum;
import ru.jblab.xmlconfig.service.ToDoService;
import ru.jblab.xmlconfig.utils.FormMapper;


@Controller
public class ToDoController {

    @Autowired
    @Qualifier("ToDoService")
    ToDoService service;
    @Autowired
    private ToDoFormValidator validator;

    @RequestMapping(value = "/todo/create", method = RequestMethod.GET)
    public String renderForm(ModelMap map) {
        map.addAttribute("addform", new TaskForm());
        map.addAttribute("taskPriorities", TaskPriorityEnum.values());
        map.addAttribute("defaultTaskPriority", TaskPriorityEnum.getDefault());
        return "todolist/createtask";
    }

    @RequestMapping(value = "/todo/add", method = RequestMethod.POST)
    public String addTask(@ModelAttribute("addform") TaskForm taskForm, BindingResult result, ModelMap map) {
        validator.validate(taskForm, result);
        if (result.hasErrors()) {
            return "/todolist/createtask";
        } else {
            service.addTask(taskForm);
            return "redirect:/todo/show";
        }
    }

    @RequestMapping(value = "/todo/addJs", method = RequestMethod.POST)
    public String addTaskJs(@ModelAttribute("addform") TaskForm taskForm, BindingResult result, ModelMap map) {
        validator.validate(taskForm, result);
        if (result.hasErrors()) {
            map.addAttribute("taskPriorities", TaskPriorityEnum.values());
            map.addAttribute("defaultTaskPriority", TaskPriorityEnum.getDefault());
            return "/todolist/createtask";
        } else {
            map.addAttribute("result", service.addTask(taskForm));
            return "todolist/task";
        }
    }

    @RequestMapping(value = "/todo/show", method = RequestMethod.GET)
    public String renderTasksList(ModelMap map) {
        map.addAttribute("todoList", service.getTasks());
        return "todolist/tasks";
    }

    @RequestMapping(value = "/todo/clean", method = RequestMethod.GET)
    public String cleanTasksList(ModelMap map) {
        service.clearTasks();
        return "redirect:/todo/show";
    }

    @RequestMapping(value = "/todo/remove/{id}", method = RequestMethod.GET)
    public String removeTask(@PathVariable("id") long id, ModelMap map) {
        service.deleteTask(id);
        return "redirect:/todo/show";
    }

    @RequestMapping(value = "/todo/edit/{id}", method = RequestMethod.GET)
    public String editTask(@PathVariable("id") long id, ModelMap map) {
        map.addAttribute("taskToEdit", FormMapper.taskToTaskForm(service.getTask(id)));
        map.addAttribute("taskPriorities", TaskPriorityEnum.values());
        return "/todolist/edittask";
    }

    @RequestMapping(value = "/todo/update/{id}", method = RequestMethod.POST)
    public String updateTask(@ModelAttribute("taskToEdit") TaskForm taskForm, @PathVariable("id") long id, BindingResult result, ModelMap map) {
        validator.validate(taskForm, result);
        if (result.hasErrors()) {
            map.addAttribute("taskPriorities", TaskPriorityEnum.values());
            return "/todolist/edittask";
        } else {
            taskForm.setId(id);
            service.updateTask(FormMapper.taskFormToTask(taskForm));
            return "redirect:/todo/show";
        }
    }

    @RequestMapping(value = "/todo/changestatus/{id}/{status}", method = RequestMethod.GET)
    public String changeStatus(@PathVariable("id") long id, @PathVariable("status") int status, ModelMap map) {
        service.changeTaskStatus(id, status);
        return "redirect:/todo/show";
    }

}
