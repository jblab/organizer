package ru.jblab.xmlconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.jblab.xmlconfig.controller.validator.FormShopValidator;
import ru.jblab.xmlconfig.form.GoodsForm;
import ru.jblab.xmlconfig.model.Goods;
import ru.jblab.xmlconfig.model.ShopList;
import ru.jblab.xmlconfig.model.User;
import ru.jblab.xmlconfig.service.ShopService;
import ru.jblab.xmlconfig.utils.AuthenticationUtil;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static ru.jblab.xmlconfig.utils.AuthenticationUtil.getAuthUser;

@Controller
public class ShopController {

    @Autowired
    private FormShopValidator valid;

    @Autowired
    @Qualifier("shopService")
    ShopService service;

    /**
     * new version
     */
    @RequestMapping(value = "/shop/lists", method = RequestMethod.GET)
    public String addList(ModelMap map) {
        User authUser = AuthenticationUtil.getAuthUser();
        List<ShopList> allShopList = service.getShopListByUserId(authUser.getId());
        map.addAttribute("lists", allShopList);
        map.addAttribute("summaryPriceMap", service.getAllFullPrice(allShopList));
        return "/shop/shop-list";
    }

    @RequestMapping(value = "/shop/create-list", method = RequestMethod.GET)
    public String createList(ModelMap map) throws ParseException {
        ShopList sl = new ShopList();
        sl.setName("Название списка(" + new SimpleDateFormat("d MMMMM, yyyy").format(new Date()) + ")");
        sl.setUser(getAuthUser());
        service.add(sl);
        map.addAttribute("shopList", sl);
        map.addAttribute("goodsForm", new GoodsForm());
        return "/shop/add-goods";
    }

    @RequestMapping(value = "/shop/list/{id}", method = RequestMethod.GET)
    public String showList(@PathVariable("id") Long id, ModelMap map) {
        ShopList shopListById = service.getShopListById(id);
        map.addAttribute("shopList", shopListById);
        map.addAttribute("productList", shopListById.getGoods());
        map.addAttribute("goodsForm", new GoodsForm());
        map.addAttribute("fullPrice", service.getFullPrice(shopListById.getGoods()));
        return "/shop/show";
    }

    @RequestMapping(value = "/shop/remove/goods{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void removeGoods(@PathVariable("id") Long id) {
        service.removeGoods(id);

    }

    @RequestMapping(value = "/shop/remove/list{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void removeShopList(@PathVariable("id") Long id) {
        service.removeList(id);
    }

    @RequestMapping(value = "/shop/add-ajax", method = RequestMethod.POST)
    public String getMinColumnPage(@ModelAttribute("shopGoods") GoodsForm goodsForm, ModelMap map) {
        Long shopListId = goodsForm.getShopListId();
        Goods goods = FormMapper.goodsFormToGoods(goodsForm);
        ShopList shopList = service.getShopListById(shopListId);
        goods.setShopListFK(shopList);
        service.add(goods);
        map.addAttribute("goods", goods);
        return "/shop/fieldTR";
    }

    @RequestMapping(value = "/shop/edit/{id}", method = RequestMethod.POST)
    public String edit(@ModelAttribute("goodsForm") GoodsForm goodsForm, @PathVariable("id") Long id, ModelMap map) {
        Goods goods = FormMapper.goodsFormToGoods(goodsForm);
        service.update(goods);
        map.addAttribute("goods", goods);
        return "/shop/fieldEDT";
    }

    @RequestMapping(value = "/shop/editNameList/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ShopList editName(@RequestParam("name") String name, @PathVariable("id") Long id) {
        ShopList list = service.getShopListById(id);
        list.setName(name);
        service.update(list);
        return list;
    }


}
