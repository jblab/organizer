package ru.jblab.xmlconfig.model.enums;

public enum ShopEnum {
    PCS("pcs"),
    KG("kg"),
    L("l"),
    BOXES("boxes"),
    BOTTLE("bottle"),
    MILLILITERS("milliliters"),
    G("g"),
    PACKAGING("packaging");

    private String value;


    ShopEnum(String value) {
        this.value = value;
    }
    public static ShopEnum[] getAll(){
        return ShopEnum.class.getEnumConstants();
    }


    public String toString() {
        return value;
    }

    public String getValue() {
        return this.value;
    }

    public String getGoodsType(){
        return this.name();
    }
}
