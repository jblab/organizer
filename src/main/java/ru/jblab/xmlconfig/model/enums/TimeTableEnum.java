package ru.jblab.xmlconfig.model.enums;

/**
 * Created by Dilya on 21.10.2014.
 */
public enum  TimeTableEnum {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private String value;

    public static TimeTableEnum[] getAll(){
        return TimeTableEnum.class.getEnumConstants();
    }

    TimeTableEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return this.value;
    }

    public static TimeTableEnum getByValue(String value) {
        for (TimeTableEnum timeTableEnum : TimeTableEnum.values()) {
            if (timeTableEnum.value.equals(value)) return timeTableEnum;
        }
        return null;
    }

    public String toString() {
        return value;
    }
    public static TimeTableEnum getDefault() {
        return MONDAY;
    }
}
