package ru.jblab.xmlconfig.model.enums;

/**
 * Created by Александр on 16.10.14.
 */
public enum FriendTypeEnum {
    FRIEND("friend"),
    SCHOOLMATE("schoolmate"),
    FAMILY("family"),
    FAMILIAR("familiar");

    private String value;

    FriendTypeEnum(String s) {
        this.value=s;
    }

    public static FriendTypeEnum[] getAll(){
        return FriendTypeEnum.class.getEnumConstants();
    }

    public String toString() {
        return value;
    }

    public String getValue() {
        return this.value;
    }

    public String getFriendType() {
        return this.name();
    }


    /**
     * Convert string to Friend enum
     *
     * @param type
     * @return
     */
    public static FriendTypeEnum stringToFriendEnum(String type){
        if(type != null){
            for (FriendTypeEnum friendTypeEnum: FriendTypeEnum.values()){
                if(type.equalsIgnoreCase(friendTypeEnum.getValue()))
                    return friendTypeEnum;
            }
        }
        return null;
    }
}
