package ru.jblab.xmlconfig.model.enums;

/**
 * Created by Arcoo on 22.10.2014.
 */
public enum KindOfSportEnum {

    WATERSPORT("water sport"),
    ATHLETICSPORT("athletic sport"),
    TEAMSPORT("team sport");

    private String value;

    KindOfSportEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static KindOfSportEnum getByValue(String value) {
        for (KindOfSportEnum kindOfSportEnum : KindOfSportEnum.values()) {
            if (kindOfSportEnum.value.equals(value)) {  //getValue();
                return kindOfSportEnum;
            }
        }
        return null;
    }

    public String toString() {
        return value;
    }

//    public static KindOfSportEnum getDefault() {
//        return ATHLETICSPORT;
//    }
}
