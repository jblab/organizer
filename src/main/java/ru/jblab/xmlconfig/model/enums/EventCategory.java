package ru.jblab.xmlconfig.model.enums;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tommoto on 17.10.14.
 */
public enum EventCategory {
    THEATER("Theater"),
    CLUBS("Clubs"),
    CINEMA("Cinema"),
    PHILARMONICS("Philarmonics"),
    SPORT("Sport"),
    EXPOSITION("Exposition"),
    CONCERT("Concert"),
    FESTIVAL("Festival"),
    WORKSHOP("Workshop");

    private String value;

    EventCategory(String value) {
        this.value = value;
    }

    public static EventCategory fromString(String value){
        for (EventCategory o : EventCategory.values()) {
              if (o.value.equals(value)) return o;
        }
        return null;
    }

    public String getValue(){
        return value;
    }

    public static List<String> getValues(){
        List<String> list = new LinkedList<String>();
        for (EventCategory ec : EventCategory.values()){
            list.add(ec.value);
        }
        return list;
    }

    public String getType(){
        return this.name();
    }
}
