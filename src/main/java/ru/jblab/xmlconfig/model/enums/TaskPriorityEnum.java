package ru.jblab.xmlconfig.model.enums;

/**
 * Created by vietto on 21.10.2014.
 */
public enum TaskPriorityEnum {

    IMPORTANT("Важный", 3),
    NORMAL("Обычный", 2),
    SIMPLE("Низкий", 1);

    private String value;
    private int priorityWeight;

    TaskPriorityEnum(String value, int weight) {
        this.value = value;
        this.priorityWeight = weight;
    }

    public String getValue() {
        return this.value;
    }

    public int getPriorityWeight() {
        return priorityWeight;
    }

    public static TaskPriorityEnum getByValue(String value) {
        for (TaskPriorityEnum taskPriorityEnum : TaskPriorityEnum.values()) {
            if (taskPriorityEnum.value.equals(value)) return taskPriorityEnum;
        }
        return null;
    }

    public String toString() {
        return value;
    }

    public static TaskPriorityEnum getDefault() {
        return NORMAL;
    }
}
