package ru.jblab.xmlconfig.model;

import javax.persistence.*;
import java.sql.Time;

/**
 * Created by Dilya on 08.10.2014.
 */
@Entity(name="Event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String eventName;
    private String weekday;
    private Time time;

/*
    public Event() {
    }

    public Event(String eventName, String weekday, Time time) {
        this.eventName = eventName;
        this.weekday = weekday;
        this.time = time;
    }
*/

    @Column(name = "eventName")
    public String getEventName() {
        return eventName;
    }

    public void setEventName(String name) {
        this.eventName = name;
    }

    @Column(name = "weekday")
    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    @Column(name = "time")
    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventName='" + eventName + '\'' +
                ", weekday='" + weekday + '\'' +
                ", time=" + time +
                '}';
    }
}
