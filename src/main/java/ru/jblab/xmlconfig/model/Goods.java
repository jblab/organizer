package ru.jblab.xmlconfig.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by admiral on 08.10.14.
 */
@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String category;
    @NotNull
    @NotEmpty
    private String nameOfProduct;
    private Integer countGoods;
    private String typeCount;
    private Integer price;
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn( updatable = false,nullable = false)
    @JsonIgnore
    private ShopList shopListFK;


    //TODO когда освобожусь, добавить кол-во продукта go to ТЗ
    public Goods(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public Goods() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public Integer getCountGoods() {
        return countGoods;
    }

    public void setCountGoods(Integer countGoods) {
        this.countGoods = countGoods;
    }

    public String getTypeCount() {
        return typeCount;
    }

    public void setTypeCount(String typeCount) {
        this.typeCount = typeCount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ShopList getShopListFK() {
        return shopListFK;
    }

    public void setShopListFK(ShopList shopListFK) {
        this.shopListFK = shopListFK;
    }
}
