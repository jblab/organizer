package ru.jblab.xmlconfig.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Rigen on 07.10.14.
 */
@Entity(name = "note_model")
@Table(name = "noteModel")
public class NoteModel {

    @EmbeddedId
    private NoteCompositeKey noteKey;

    @Column(name = "noteText")
    private String noteText;

    public NoteModel(NoteCompositeKey noteKey, String noteText) {
        this.noteKey = noteKey;
        this.noteText = noteText;
    }


    public NoteModel() {
    }

    @ModelAttribute
    public NoteModel getNote() {
        return new NoteModel();
    }

    @JsonSerialize(using = StringSerializer.class)
    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public NoteCompositeKey getNoteKey() {
        return noteKey;
    }

    public void setNoteKey(NoteCompositeKey noteKey) {
        this.noteKey = noteKey;
    }
}
