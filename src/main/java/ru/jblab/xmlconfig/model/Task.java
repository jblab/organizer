package ru.jblab.xmlconfig.model;

import ru.jblab.xmlconfig.model.enums.TaskPriorityEnum;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
public class Task implements Comparable<Task> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String taskname;
    private String priority;
    private String comment;
    private Date date;
    private Date deadline;
    private int priorityIndex;
    private int redacted;

    private int status;


    public int getRedacted() {
        return redacted;
    }

    public void setRedacted(int redacted) {
        this.redacted = redacted;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Task(String taskname, String priority, String comment, Date deadline) {
        this.taskname = taskname;
        this.priority = priority;
        priorityIndex = TaskPriorityEnum.getByValue(priority).getPriorityWeight();
        this.comment = comment;
        date = new Date();
        this.deadline = deadline;
    }

    public Task() {
        date = new Date();
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
        priorityIndex = TaskPriorityEnum.getByValue(priority).getPriorityWeight();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
        return format.format(date);
    }

    public String getDeadlineString() {

        if (deadline != null) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm dd.MM.yyyy");
            return format.format(deadline);
        } else {
            return "";
        }
    }

    public boolean isFailed() {
        if (deadline == null) return false;
        return new Date().after(deadline);
    }

    public void setFailed(boolean failed) {
        boolean failed1 = new Date().after(deadline);
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskname='" + taskname + '\'' +
                ", priority='" + priority + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }

    @Override
    public int compareTo(Task o) {
        if (this.priorityIndex == o.priorityIndex) {
            return this.date.compareTo(o.date);
        } else {
            return o.priorityIndex - this.priorityIndex;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPriorityIndex() {
        return priorityIndex;
    }

    public void setPriorityIndex(int priorityIndex) {
        this.priorityIndex = priorityIndex;
    }
}
