package ru.jblab.xmlconfig.model;

import ru.jblab.xmlconfig.model.enums.KindOfSportEnum;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Arcoo on 08.10.2014.
 */
@Entity
public class KindOfSport {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date")
    private Long date;
    @Column(name = "name")
    private String name;
    @Column(name = "time")
    private Double time;
    @Column(name = "metric")
    private String metric;
    @Enumerated(EnumType.STRING)
    private KindOfSportEnum sportsKindEnum;

    private String value;

    public String getValue() {
        return sportsKindEnum.getValue();
    }

    public KindOfSportEnum getSportsKindEnum() {
        return sportsKindEnum;
    }

    public void setSportsKindEnum(KindOfSportEnum sportsKindEnum) {
        this.sportsKindEnum = sportsKindEnum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getDateStr() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(date)); // or .getTime()
    }

}
