package ru.jblab.xmlconfig.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Александр on 30.10.14.
 */
@Entity(name = "user_table")
@Table(name = "user_table")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "user_login")
    private String name;
    @Column(name = "user_password")
    private String password;
    private String salt;

    @OneToMany(mappedBy = "shopUser", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<ShopList> shopLists;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setShopLists(List<ShopList> shopLists) {
        this.shopLists = shopLists;
    }

    public List<ShopList> getShopLists() {
        return shopLists;
    }
}
