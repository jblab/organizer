package ru.jblab.xmlconfig.model;

import javax.persistence.*;


@Entity(name = "users_events")
//@Embeddable
@Table
public class UserEventBind {
    enum EventStatus{
        CERTAINLY_YES, MAYBE_YES, CERTAINLY_NO //dlya zaprosov luchwie imena, ya schitayu
    }
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "userName")
//    @ManyToOne(fetch = FetchType.LAZY)
    private Long userName;

    @Column(name = "eventId")
//    @ManyToOne(fetch = FetchType.LAZY)
    private Long eventId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EventStatus status;


    public Long getUserName() {
        return userName;
    }

    public void setUserName(Long userName) {
        this.userName = userName;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @Enumerated
    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
