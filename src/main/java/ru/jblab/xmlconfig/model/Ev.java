package ru.jblab.xmlconfig.model;


import ru.jblab.xmlconfig.model.enums.EventCategory;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tommoto on 08.10.14.
 *
 */
@Entity(name = "ev_model")
@SecondaryTable(name = "users_events", pkJoinColumns = {
        @PrimaryKeyJoinColumn(name = "", referencedColumnName = "")
})
@Table(name="ev")
public class Ev {
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");

    @Id
//    @OneToMany(mappedBy = "ev")
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="date")
    private Long date;

    @Column(name="description")
    private String description;

    @Column(name="price")
    private String price;

    @Column(name="category")
    @Enumerated(EnumType.STRING)
    private EventCategory category;

    //becomes one word longer than 140, if we need <140, use lastindexof instead
    private String descriptionPreview;
    public String getDescriptionPreview(){
        final int DESCRIPTION_PREVIEW_CHARACTERS = 140;
        if (description == null ||
                description.length() < DESCRIPTION_PREVIEW_CHARACTERS) {
            return description;
        }else {
            int lastSpace = description.indexOf(' ', DESCRIPTION_PREVIEW_CHARACTERS);
            return description.substring(0, lastSpace);
        }
    }
    public String getFdate() {
        return sdf.format(new Date(date));
    }
    private String fdate; //used in JSP
    private static Long idGen = Long.valueOf(0);
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Ev() {
    }

    @Enumerated
    public EventCategory getCategory() {
        return category;
    }

    public void setCategory(EventCategory category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Ev(String name, Long date, String description, String category) {
        this.name = name;
        this.date = date;
        this.description = description;
        this.category = EventCategory.fromString(category);
        this.id = ++idGen;
    }
}