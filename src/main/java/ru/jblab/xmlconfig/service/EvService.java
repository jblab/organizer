package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.model.Ev;
import ru.jblab.xmlconfig.model.enums.EventCategory;

import java.util.List;

/**
 * Created by tommoto on 10.10.14.
 */
public interface EvService {
    public List<Ev> getEventsSortedList();
    public void addEvent(Ev event);
    public void deleteEventAt(Long eventId);
    public Ev getEventAt(Long eventId);
    public void update(Ev event);
    public List<Ev> getNames();
    public List<Ev> getEventsAtPage(int page, EventCategory category);
    public long count();
}
