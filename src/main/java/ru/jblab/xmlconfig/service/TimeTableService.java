package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.form.EventForm;
import ru.jblab.xmlconfig.model.Event;

import java.util.List;
import java.util.Map;


/**
 * Created by Dilya on 10.10.2014.
 */
public interface TimeTableService {

    void addEvent(EventForm eventForm);

    List<Event> getEvents();

    void deleteEvent(Long id);

    Event searchEvent(Long id);

    void edit(EventForm eventForm);

    public Map<String, List<Event>> getAllEvents();

    public List<Event> getEventsByDay(String weekday);
}
