package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.IKindOfSportDao;
import ru.jblab.xmlconfig.form.KindOfSportForm;
import ru.jblab.xmlconfig.model.KindOfSport;
import ru.jblab.xmlconfig.model.enums.KindOfSportEnum;
import ru.jblab.xmlconfig.service.ISportResultService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.text.ParseException;
import java.util.*;

/**
 * Created by Arcoo on 10.10.2014.
 */
@Service("WithDBSportRes")
@Transactional
public class SportResultServiceImpl implements ISportResultService {

    @Autowired
    IKindOfSportDao kindOfSportDao;

    @Override
    @Transactional
    public List<KindOfSport> getKindsList() {
        return kindOfSportDao.findAll();
    }

    @Override
    @Transactional
    public KindOfSport addSportRes(KindOfSportForm kindOfSportForm) throws ParseException {
        KindOfSport kindOfSport = FormMapper.kindOfSportFormToKindOfSport(kindOfSportForm);
        kindOfSportDao.save(kindOfSport);
        return kindOfSport;
    }

    @Override
    @Transactional
    public void editSportRes(KindOfSportForm kindOfSportForm) throws ParseException {
        KindOfSport kindOfSport = FormMapper.kindOfSportFormToKindOfSport(kindOfSportForm);
        kindOfSportDao.edit(kindOfSport);
    }

    @Override
    @Transactional
    public KindOfSport searchKindOfSportById(Long id) {      // str = name;
        return kindOfSportDao.findById(id);
    }

    @Override
    @Transactional
    public void deleteKindOfSport(Long id) {
        kindOfSportDao.delete(id);
    }

    @Override
    @Transactional
    public void clearListOfSportResults() {
        kindOfSportDao.deleteAll();
    }

    @Override
    @Transactional
    public Map<String, List<KindOfSport>> getAllSportResults() {
        List<KindOfSport> list = kindOfSportDao.findAll();
        Map<String, List<KindOfSport>> map = new HashMap<>();
        for (KindOfSportEnum kindOfSportEnum : KindOfSportEnum.values()) {
            Iterator iterator = list.iterator();
            map.put(kindOfSportEnum.getValue(), new LinkedList<KindOfSport>());     // Creating EnumLists.
            while (iterator.hasNext()) {
                KindOfSport kindOfSport = (KindOfSport) iterator.next();
                if (kindOfSport.getSportsKindEnum().equals(kindOfSportEnum.getValue())) {       // !!!!!!!!
                    map.get(kindOfSportEnum.getValue()).add(kindOfSport);
                }
            }
        }
        return map;
    }

    @Override
    @Transactional
    public List<KindOfSport> findSportResultsByDate(Long date) {
        return kindOfSportDao.findListByDate(date);
    }

    @Override
    @Transactional
    public void /*List<KindOfSport>*/ deleteSpecificListByDate(Long date) {
        kindOfSportDao.deleteAllByDate(date);
    }


}
