package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.UserDao;
import ru.jblab.xmlconfig.form.UserForm;
import ru.jblab.xmlconfig.model.User;
import ru.jblab.xmlconfig.service.UserService;
import ru.jblab.xmlconfig.utils.FormMapper;
import ru.jblab.xmlconfig.utils.HelperUtil;

/**
 * Created by Александр on 30.10.14.
 */
@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public User findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    public User addUser(UserForm userForm) {
        String salt = HelperUtil.generateSalt(userForm.getPassword().length());
        userForm.setSalt(salt);
        User user = FormMapper.userFormToUser(userForm);
        String password = user.getPassword();
        user.setPassword(HelperUtil.md5(password, salt));
        userDao.save(user);
        return user;
    }

}
