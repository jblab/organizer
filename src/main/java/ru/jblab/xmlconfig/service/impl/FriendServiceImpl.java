package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.FriendDao;
import ru.jblab.xmlconfig.form.FriendForm;
import ru.jblab.xmlconfig.model.Friend;
import ru.jblab.xmlconfig.model.enums.FriendTypeEnum;
import ru.jblab.xmlconfig.service.FriendService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.util.*;

/**
 * Created by Александр on 10.10.14.
 */
@Service("friend")
@Transactional
public class FriendServiceImpl implements FriendService {

    @Autowired
    FriendDao friendDao;

    @Override
    public void addFriend(FriendForm friendForm) {
        Friend friend = FormMapper.friendFromToFriend(friendForm);
        friendDao.save(friend);
//        System.out.println(friend.getType());
//        System.out.println(friendDao.findAll().toString());
    }

    @Override
    public void setTypeFriend(String id, String type) {
        Friend friend = friendDao.findById(Long.parseLong(id));
        friend.setType(FriendTypeEnum.stringToFriendEnum(type));
        friendDao.setByFriend(friend);
    }

    @Override
    public void deleteFriend(String id) {
        friendDao.delete(Long.parseLong(id));
    }

    @Override
    public Map<String, List<Friend>> getAllFriend() {
        List<Friend> list = friendDao.findAll();
        Map<String, List<Friend>> map = new HashMap<>();
        for (FriendTypeEnum friendTypeEnum : FriendTypeEnum.values()) {
            Iterator iterator = list.iterator();
            map.put(friendTypeEnum.getValue(), new LinkedList<Friend>());
            while (iterator.hasNext()) {
                Friend friend = (Friend) iterator.next();
                if (friend.getType().equals(friendTypeEnum.getValue())){
                        map.get(friendTypeEnum.getValue()).add(friend);
                }
            }
        }
        return map;
    }
}
