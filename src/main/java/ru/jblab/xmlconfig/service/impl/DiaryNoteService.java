package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.NoteModelDao;
import ru.jblab.xmlconfig.form.NoteForm;
import ru.jblab.xmlconfig.model.NoteCompositeKey;
import ru.jblab.xmlconfig.model.NoteModel;
import ru.jblab.xmlconfig.model.User;
import ru.jblab.xmlconfig.service.DiaryService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rigen on 10.10.14.
 */
@Service("NoteService")
public class DiaryNoteService implements DiaryService {

    @Autowired
    NoteModelDao noteModelDao;

    @Override
    @Transactional
    public void addNote(NoteModel note) {
        noteModelDao.save(note);

    }

    @Override
    @Transactional
    public void editNote(NoteModel note) {
        noteModelDao.update(note);
    }

    @Override
    @Transactional
    public void deleteNote(NoteCompositeKey noteKey) {
        noteModelDao.delete(noteKey);
    }

    @Override
    @Transactional
    public List<NoteModel> getNotesList() {
        return noteModelDao.findAll();
    }

    @Override
    @Transactional
    public NoteModel findNoteById(NoteCompositeKey noteKey) {
        return noteModelDao.findById(noteKey);
    }

    @Override
    @Transactional
    public NoteModel findNoteByDate(Long date) {
        return noteModelDao.findByDate(date);
    }

    @Override
    @Transactional
    public NoteModel findNoteByPK(Long date, User user) {
        return noteModelDao.findByNotePK(date, user);
    }

    @Override
    @Transactional
    public List<NoteForm> getNotesFormList() {
        List<NoteModel> modelList = noteModelDao.findAllAndSortBy("noteKey.date", false);
        List<NoteForm> formList = new ArrayList<>();
        for (NoteModel note : modelList) {
            formList.add(FormMapper.noteToNoteForm(note));
        }
        return formList;
    }

}
