package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.TodoModelDao;
import ru.jblab.xmlconfig.form.TaskForm;
import ru.jblab.xmlconfig.model.Task;
import ru.jblab.xmlconfig.service.ToDoService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.util.List;

/**
 * Created by root on 10.10.14.
 */
@Service("ToDoService")
@Transactional
public class ToDoServiceImpl implements ToDoService {

    @Autowired
    TodoModelDao todoModelDao;

    public Task addTask(TaskForm taskForm) {
        Task task = FormMapper.taskFormToTask(taskForm);
        todoModelDao.save(task);
        return task;
    }

    public List<Task> getTasks() {
        return todoModelDao.findAllAndSortTasks();
    }

    @Override
    public void deleteTask(long id) {
        todoModelDao.delete(id);
    }

    @Override
    public void clearTasks() {
        todoModelDao.deleteAll();
    }

    @Override
    public void updateTask(Task task) {
        todoModelDao.update(task);
    }

    @Override
    public Task getTask(long id) {
        return todoModelDao.findById(id);
    }

    @Override
    public void changeTaskStatus(long id, int status) {
        Task task = getTask(id);
        task.setStatus(status);
        updateTask(task);
    }


}
