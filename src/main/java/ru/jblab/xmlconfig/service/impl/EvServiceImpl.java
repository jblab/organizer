package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.EvModelDao;
import ru.jblab.xmlconfig.model.Ev;
import ru.jblab.xmlconfig.model.enums.EventCategory;
import ru.jblab.xmlconfig.service.EvService;

import java.util.List;


@Service("httpEv")
@Transactional
public class EvServiceImpl implements EvService {

    @Autowired
    EvModelDao evDao;

    @Override
    public List<Ev> getEventsSortedList() {
        List<Ev> list = evDao.findAllAndSortBy("date", false);
        return list;
    }
    @Override
    public void addEvent(Ev event) {
        evDao.save(event);
    }

    @Override
    public Ev getEventAt(Long eventId) {
        return evDao.findById(eventId);
    }

    @Override
    public void update(Ev newEvent) {
        evDao.update(newEvent);
    }

    @Override
    public List<Ev> getNames() {
        //for search
        return null;
    }

    @Override
    public List<Ev> getEventsAtPage(int page, EventCategory category) {
        final int maxResults = 10;
        return evDao.getEventsPage(category, (page - 1) * maxResults, maxResults);
    }

    @Override
    public long count() {
        return evDao.count();
    }

    @Override
    public void deleteEventAt(Long eventId) {
        evDao.delete(eventId);
    }
}