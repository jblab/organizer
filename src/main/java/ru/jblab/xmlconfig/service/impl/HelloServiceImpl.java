package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.HelloModelDao;
import ru.jblab.xmlconfig.model.HelloModel;
import ru.jblab.xmlconfig.service.HelloService;

import java.util.List;

/**
 * Created by ainurminibaev on 11.10.14.
 */
@Service
public class HelloServiceImpl implements HelloService {
    @Autowired
    HelloModelDao helloModelDao;

    @Override
    @Transactional
    public HelloModel save(HelloModel model) {
        helloModelDao.save(model);
        return model;
    }

    @Override
    @Transactional
    public List<HelloModel> findAll() {
        return helloModelDao.findAll();
    }
}
