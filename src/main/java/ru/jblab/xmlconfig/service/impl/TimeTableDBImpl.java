package ru.jblab.xmlconfig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.TimeTableDao;
import ru.jblab.xmlconfig.form.EventForm;
import ru.jblab.xmlconfig.model.Event;
import ru.jblab.xmlconfig.model.enums.TimeTableEnum;
import ru.jblab.xmlconfig.service.TimeTableService;
import ru.jblab.xmlconfig.utils.FormMapper;

import java.util.*;

/**
 * Created by Dilya on 17.10.2014.
 */
@Service("DB")
@Transactional
public class TimeTableDBImpl implements TimeTableService {

    @Autowired
    TimeTableDao timeTableDao;

    @Override
    public void addEvent(EventForm eventForm) {
        Event event = FormMapper.eventFormToEvent(eventForm);
        timeTableDao.save(event);
    }
    public void edit(EventForm eventForm) {
        Event event = FormMapper.eventFormToEvent(eventForm);
        timeTableDao.edit(event);
    }

    public Map<String, List<Event>> getAllEvents() {
        List<Event> list = timeTableDao.findAll();
        Map<String, List<Event>> map = new HashMap<>();
        for (TimeTableEnum timeTableEnum : TimeTableEnum.values()) {
            Iterator iterator = list.iterator();
            map.put(timeTableEnum.getValue(), new LinkedList<Event>());
            while (iterator.hasNext()) {
                Event event = (Event) iterator.next();
                if (event.getWeekday().equals(timeTableEnum.getValue())){
                    map.get(timeTableEnum.getValue()).add(event);
                }
            }
        }
        return map;
    }



    public List<Event> getEventsByDay(String weekday) {
        return timeTableDao.getByWeekday(weekday);
    }
    @Override
    public List<Event> getEvents() {
        return timeTableDao.findAll();
    }

    @Override
    public void deleteEvent(Long id) {
        timeTableDao.delete(id);
    }

    @Override
    public Event searchEvent(Long id) {
        return timeTableDao.findById(id);
    }


}
