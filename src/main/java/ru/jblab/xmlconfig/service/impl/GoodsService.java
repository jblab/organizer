package ru.jblab.xmlconfig.service.impl;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jblab.xmlconfig.dao.GoodsDao;
import ru.jblab.xmlconfig.dao.ShopListDao;
import ru.jblab.xmlconfig.dao.UserDao;
import ru.jblab.xmlconfig.model.Goods;
import ru.jblab.xmlconfig.model.ShopList;
import ru.jblab.xmlconfig.service.ShopService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("shopService")
@Transactional
public class GoodsService implements ShopService {

    @Autowired
    GoodsDao goodsDao;

    @Autowired
    ShopListDao shopListDao;

    @Autowired
    UserDao userDao;

    @Override
    public void add(Goods goods) {
        this.goodsDao.save(goods);
    }

    @Override
    public List<Goods> showAll() {
        return goodsDao.findAll();
    }

    @Override
    public void deleteAll() {
        goodsDao.deleteAll();
    }

    @Override
    public void removeGoods(long id) {
        goodsDao.delete(id);
    }

    @Override
    public void update(Goods goods) {
        this.goodsDao.update(goods);
    }

    @Override
    public Goods getGoods(Long id) {
        return goodsDao.findById(id);
    }

    @Override
    public List<ShopList> getAllShopList() {
        List<ShopList> all = shopListDao.findAll();
        for (ShopList shopList : all) {
            Hibernate.initialize(shopList.getGoods());
        }
        return all;
    }

    @Override
    public ShopList getShopListById(Long id) {
        ShopList shopList = shopListDao.findById(id);

        Hibernate.initialize(shopList.getGoods());

        return shopList;
    }

    @Override
    public void add(ShopList sl) {
        this.shopListDao.save(sl);
    }

    @Override
    public void update(ShopList list) {
        this.shopListDao.update(list);
    }

    @Override
    public void removeList(Long id) {
        this.shopListDao.delete(id);
    }

    @Override
    public Map<Long, Integer> getAllFullPrice(List<ShopList> allShopList) {
        Map<Long, Integer> fullPrice = new HashMap<>();
        for (ShopList l : allShopList) {
            fullPrice.put(l.getId(), summer(l.getGoods()));
        }
        return fullPrice;
    }

    @Override
    public Integer getFullPrice(List<Goods> list) {
        return summer(list);
    }

    @Override
    public List<ShopList> getShopListByUserId(Long userId) {
        List<ShopList> shopListByUserId = shopListDao.getShopListByUserId(userId);
        for (ShopList l : shopListByUserId) {
            Hibernate.initialize(l.getGoods());
        }
        return shopListByUserId;
    }

    private Integer summer(List<Goods> list) {
        int sum = 0;
        for (Goods g : list) {
            sum += g.getPrice();
        }
        return sum;
    }


}

