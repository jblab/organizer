package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.model.Goods;
import ru.jblab.xmlconfig.model.ShopList;

import java.util.List;
import java.util.Map;

public interface ShopService {

    void add(Goods goods);

    List<Goods> showAll();

    void deleteAll();

    void removeGoods(long id);


    void update(Goods goods);

    Goods getGoods(Long id);

    List<ShopList> getAllShopList();

    ShopList getShopListById(Long id);

    void add(ShopList sl);

    void update(ShopList list);

    void removeList(Long id);

    Map<Long,Integer> getAllFullPrice(List<ShopList> allShopList);

    Integer getFullPrice(List<Goods> list);

    List<ShopList> getShopListByUserId(Long userId);
}
