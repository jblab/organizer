package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.form.KindOfSportForm;
import ru.jblab.xmlconfig.model.KindOfSport;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Arcoo on 10.10.2014.
 */
public interface ISportResultService {

    KindOfSport addSportRes(KindOfSportForm kindOfSportForm) throws ParseException;

    void deleteKindOfSport(Long id); //deleteById

    void clearListOfSportResults();

    void editSportRes(KindOfSportForm kindOfSportForm) throws ParseException;

    KindOfSport searchKindOfSportById(Long id);

    List<KindOfSport> getKindsList();

    List<KindOfSport> findSportResultsByDate(Long date);        // !!!

    void /*List<KindOfSport>*/ deleteSpecificListByDate(Long date);

    Map<String, List<KindOfSport>> getAllSportResults();

}
