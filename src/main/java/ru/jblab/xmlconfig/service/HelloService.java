package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.model.HelloModel;

import java.util.List;

/**
 * Created by ainurminibaev on 11.10.14.
 */
public interface HelloService {

    HelloModel save(HelloModel model);

    List<HelloModel> findAll();
}
