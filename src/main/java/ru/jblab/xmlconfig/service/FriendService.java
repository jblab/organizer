package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.form.FriendForm;
import ru.jblab.xmlconfig.model.Friend;

import java.util.List;
import java.util.Map;

/**
 * Created by Александр on 10.10.14.
 */
public interface FriendService {

    public void addFriend(FriendForm friendForm);

    public void setTypeFriend(String id, String type);

    public void deleteFriend(String id);

    public Map<String, List<Friend>> getAllFriend();
}
