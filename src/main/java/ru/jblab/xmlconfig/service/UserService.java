package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.form.UserForm;
import ru.jblab.xmlconfig.model.User;

/**
 * Created by Александр on 30.10.14.
 */
public interface UserService {

     User findByName(String name);

     User addUser(UserForm userForm);

}
