package ru.jblab.xmlconfig.service;

import org.springframework.stereotype.Service;
import ru.jblab.xmlconfig.form.TaskForm;
import ru.jblab.xmlconfig.model.Task;

import java.util.List;


@Service
public interface ToDoService {

    public Task addTask(TaskForm taskForm);

    public List<Task> getTasks();

    public void deleteTask(long id);

    public void clearTasks();

    public void updateTask(Task task);

    public Task getTask(long id);

    public void changeTaskStatus(long id, int status);

}
