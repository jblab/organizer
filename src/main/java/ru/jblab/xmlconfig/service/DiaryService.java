package ru.jblab.xmlconfig.service;

import ru.jblab.xmlconfig.form.NoteForm;
import ru.jblab.xmlconfig.model.NoteCompositeKey;
import ru.jblab.xmlconfig.model.NoteModel;
import ru.jblab.xmlconfig.model.User;

import java.util.List;

/**
 * Created by Rigen on 10.10.14.
 */

public interface DiaryService {
    public void addNote(NoteModel note);

    public void editNote(NoteModel note);

    public void deleteNote(NoteCompositeKey noteKey);

    public List<NoteModel> getNotesList();

    public List<NoteForm> getNotesFormList();

    public NoteModel findNoteById(NoteCompositeKey noteKey);

    public NoteModel findNoteByDate(Long date);

    public NoteModel findNoteByPK(Long date, User user);
}
