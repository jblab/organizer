package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.Goods;

public interface GoodsDao extends AbstractDao<Goods, Long> {

}
