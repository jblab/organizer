package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.Task;

import java.util.List;

/**
 * Created by root on 16.10.14.
 */
public interface TodoModelDao extends AbstractDao<Task, Long> {

    List<Task> findAllAndSortTasks();

}
