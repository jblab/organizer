package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.Ev;
import ru.jblab.xmlconfig.model.enums.EventCategory;

import java.util.List;

/**
 * Created by tommoto on 15.10.14.
 */
public interface EvModelDao extends AbstractDao<Ev, Long>{
    List<Ev> findAllInCategory (EventCategory category);

    List<Ev> getEventsPage(EventCategory category, int startPosition, int maxNumber);
}
