package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.User;

/**
 * Created by Александр on 30.10.14.
 */

public interface UserDao extends AbstractDao<User, Long> {

    User findByName(String name);
}
