package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.NoteCompositeKey;
import ru.jblab.xmlconfig.model.NoteModel;
import ru.jblab.xmlconfig.model.User;

/**
 * Created by Rigen on 15.10.14.
 */
public interface NoteModelDao extends AbstractDao<NoteModel, NoteCompositeKey> {

    NoteModel findByDate(Long date);

    NoteModel findByNotePK(Long date, User user);

}
