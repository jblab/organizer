package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.Friend;

import java.util.List;

/**
 * Created by Александр on 15.10.14.
 */
public interface FriendDao extends AbstractDao<Friend, Long> {

    List<Friend> findByName(String name);

    void setByFriend(Friend friend);

}
