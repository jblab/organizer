package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.ShopList;

import java.util.List;

public interface ShopListDao extends AbstractDao<ShopList, Long> {

    List<ShopList> getShopListByUserId(Long userId);
}
