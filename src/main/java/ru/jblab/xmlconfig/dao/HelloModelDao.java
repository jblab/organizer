package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.HelloModel;

import java.util.List;

/**
 * Created by ainurminibaev on 11.10.14.
 */
public interface HelloModelDao extends AbstractDao<HelloModel, Long> {
    //Here i can mention new method's like findByName or others

    /**
     * Find all HelloModels by some name
     */
    List<HelloModel> findByName(String name);
}
