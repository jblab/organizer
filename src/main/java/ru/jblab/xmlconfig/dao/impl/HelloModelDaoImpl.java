package ru.jblab.xmlconfig.dao.impl;

import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.HelloModelDao;
import ru.jblab.xmlconfig.model.HelloModel;

import java.util.List;

/**
 * Created by ainurminibaev on 11.10.14.
 */
@Repository
public class HelloModelDaoImpl extends AbstractDaoImpl<HelloModel, Long> implements HelloModelDao {

    public HelloModelDaoImpl() {
        super(HelloModel.class);
    }

    @Override
    public List<HelloModel> findByName(String name) {
        //TODO write right implementation using HQL or Hibernate Criteria
        return getCurrentSession().createCriteria(getEntityName()).list();
    }


    //and here i can implement assigned methods
}
