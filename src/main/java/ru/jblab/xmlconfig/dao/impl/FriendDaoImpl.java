package ru.jblab.xmlconfig.dao.impl;

import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.FriendDao;
import ru.jblab.xmlconfig.model.Friend;

import java.util.List;

/**
 * Created by Александр on 15.10.14.
 */
@Repository
public class FriendDaoImpl extends AbstractDaoImpl<Friend, Long> implements FriendDao {

    public FriendDaoImpl() {
        super(Friend.class);
    }

    @Override
    public List<Friend> findByName(String name) {
        return getCurrentSession().createCriteria(getEntityName()).list();

    }

    @Override
    public void setByFriend(Friend friend) {
        getCurrentSession().update(friend);
    }
}
