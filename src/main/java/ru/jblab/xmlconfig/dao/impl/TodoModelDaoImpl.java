package ru.jblab.xmlconfig.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.TodoModelDao;
import ru.jblab.xmlconfig.model.Task;

import java.util.List;

/**
 * Created by root on 16.10.14.
 */
@Repository
public class TodoModelDaoImpl extends AbstractDaoImpl<Task, Long> implements TodoModelDao {

    protected TodoModelDaoImpl() {
        super(Task.class);
    }

    @Override
    public List<Task> findAllAndSortTasks() {
        Criteria criteria = getCurrentSession().createCriteria(Task.class);
        criteria.addOrder(Order.desc("priorityIndex"));
        criteria.addOrder(Order.asc("date"));
        return criteria.list();
    }
}
