package ru.jblab.xmlconfig.dao.impl;

import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.GoodsDao;
import ru.jblab.xmlconfig.model.Goods;


@Repository
public class GoodsDaoImpl extends AbstractDaoImpl<Goods, Long> implements GoodsDao {

    public GoodsDaoImpl() {
        super(Goods.class);
    }



}
