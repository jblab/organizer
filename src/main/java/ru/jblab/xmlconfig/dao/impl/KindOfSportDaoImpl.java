package ru.jblab.xmlconfig.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.IKindOfSportDao;
import ru.jblab.xmlconfig.model.KindOfSport;

import java.util.List;

/**
 * Created by Arcoo on 15.10.2014.
 */

@Repository
public class KindOfSportDaoImpl extends AbstractDaoImpl<KindOfSport, Long> implements IKindOfSportDao {
    public KindOfSportDaoImpl() {
        super(KindOfSport.class);
    }

    @Override
    public List<KindOfSport> findListByDate(Long date) {
        Criteria criteria = getCurrentSession().createCriteria(KindOfSport.class);
        // criteria.setMaxResults(5);
        List<KindOfSport> list = (List<KindOfSport>) criteria.add(Restrictions.eq("date", date)).list();
        if (list.size() != 0) {
            return list;
        } else {
            return null;
        }
    }

//    @Override
//    public void deleteAllByDate(Long date) {
//        Criteria criteria = getCurrentSession().createCriteria(KindOfSport.class);
//        List<KindOfSport> list = (List<KindOfSport>) criteria.add(Restrictions.eq("date", date)).list();
//        getCurrentSession().delete(list);
//    }

    @Override
    public void deleteAllByDate(Long date) {
        Query query = getCurrentSession().createQuery("delete from " + getEntityName() + " e where e.date = :date");
        query.setParameter("date", date);
        query.executeUpdate();
    }

}
