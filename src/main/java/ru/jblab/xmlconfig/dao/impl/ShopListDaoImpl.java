package ru.jblab.xmlconfig.dao.impl;

import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.ShopListDao;
import ru.jblab.xmlconfig.model.ShopList;

import java.util.List;

@Repository
public class ShopListDaoImpl extends  AbstractDaoImpl<ShopList,Long> implements ShopListDao{

    public ShopListDaoImpl() {
        super(ShopList.class);
    }

    @Override
    public List<ShopList> getShopListByUserId(Long userId) {
        return (List<ShopList>) getCurrentSession().createQuery("select l from "+ getEntityName()+" as l where l.shopUser="+userId).list();
    }
}
