package ru.jblab.xmlconfig.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.UserDao;
import ru.jblab.xmlconfig.model.User;

/**
 * Created by Александр on 30.10.14.
 */
@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, Long> implements UserDao {

    protected UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User findByName(String name) {
        return (User) getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
    }
}
