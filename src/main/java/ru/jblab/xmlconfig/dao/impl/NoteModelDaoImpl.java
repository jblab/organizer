package ru.jblab.xmlconfig.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.NoteModelDao;
import ru.jblab.xmlconfig.model.NoteCompositeKey;
import ru.jblab.xmlconfig.model.NoteModel;
import ru.jblab.xmlconfig.model.User;

import java.util.List;

/**
 * Created by Rigen on 15.10.14.
 */
@Repository
public class NoteModelDaoImpl extends AbstractDaoImpl<NoteModel, NoteCompositeKey> implements NoteModelDao {
    protected NoteModelDaoImpl() {
        super(NoteModel.class);
    }

    @Override
    public NoteModel findByDate(Long date) {
        Criteria criteria = getCurrentSession().createCriteria(NoteModel.class);
        criteria.setMaxResults(1);
        List<NoteModel> list = (List<NoteModel>) criteria.add(Restrictions.eq("noteKey.date", date)).list();
        if (list.size() != 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public NoteModel findByNotePK(Long date, User user) {
        Criteria criteria = getCurrentSession().createCriteria(NoteModel.class);
        criteria.setMaxResults(1);
        List<NoteModel> list = (List<NoteModel>) criteria.add(Restrictions.eq("noteKey.date", date)).
                add(Restrictions.eq("noteKey.user", user)).list();
        if (list.size() != 0) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
