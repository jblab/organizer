package ru.jblab.xmlconfig.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.EvModelDao;
import ru.jblab.xmlconfig.model.Ev;
import ru.jblab.xmlconfig.model.enums.EventCategory;

import java.util.List;

/**
 * Created by tommoto on 15.10.14.
 */
@Repository
public class EvModelDaoImpl extends AbstractDaoImpl<Ev, Long> implements EvModelDao {

    public EvModelDaoImpl(){
        super(Ev.class);
    }

    @Override
    public List<Ev> findAllInCategory(EventCategory category) {
        Criteria criteria = getCurrentSession().createCriteria(Ev.class);
        criteria.addOrder(Order.desc("date"));
        criteria.add(Restrictions.like("category", category));
        return criteria.list();
    }

    @Override
    public List<Ev> getEventsPage(EventCategory category, int startPosition, int itemsPerPage) {
        Criteria criteria = getCurrentSession().createCriteria(Ev.class)
                .setFirstResult(startPosition)
                .setMaxResults(itemsPerPage);
        if (category != null) criteria.add(Restrictions.like("category", category));
        return criteria.list();
    }

}