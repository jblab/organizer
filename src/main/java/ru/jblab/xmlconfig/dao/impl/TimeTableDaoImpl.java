package ru.jblab.xmlconfig.dao.impl;

import org.springframework.stereotype.Repository;
import ru.jblab.xmlconfig.dao.TimeTableDao;
import ru.jblab.xmlconfig.model.Event;

import java.util.List;
import org.hibernate.Query;
/**
 * Created by Dilya on 16.10.2014.
 */
@Repository
public class TimeTableDaoImpl extends AbstractDaoImpl<Event, Long> implements TimeTableDao {
    public TimeTableDaoImpl() {
        super(Event.class);
    }

    @Override
    public List<Event> getByWeekday(String weekday) {
       Query query =getCurrentSession().createQuery("select e from " + getEntityName() + " e where e.weekday = :weekday");
        query.setParameter("weekday", weekday);
        return (List<Event>) query.list();
    }
}
