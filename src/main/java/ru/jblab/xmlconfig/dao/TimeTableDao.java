package ru.jblab.xmlconfig.dao;

import ru.jblab.xmlconfig.model.Event;

import java.util.List;

/**
 * Created by Dilya on 14.10.2014.
 */
public interface TimeTableDao extends AbstractDao<Event, Long> {

    List<Event> getByWeekday(String weekday);
}
