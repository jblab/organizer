package ru.jblab.xmlconfig.dao;


import ru.jblab.xmlconfig.model.KindOfSport;

import java.util.List;

/**
 * Created by Arcoo on 15.10.2014.
 */
public interface IKindOfSportDao extends AbstractDao<KindOfSport, Long> {

    List<KindOfSport> findListByDate(Long date);

    void /*List<KindOfSport>*/ deleteAllByDate(Long date);

}
