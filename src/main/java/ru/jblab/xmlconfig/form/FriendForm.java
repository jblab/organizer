package ru.jblab.xmlconfig.form;

import ru.jblab.xmlconfig.model.enums.FriendTypeEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Created by Александр on 10.10.14.
 */
public class FriendForm {

    private Long id;
    private String name;
//    @Enumerated(EnumType.STRING)
    private FriendTypeEnum type;
    private String telephone;
    private String email;
    private String bdate;
    private String vk;
    private String instagram;
    private String skype;
    private String facebook;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }

    public String getVk() {
        return vk;
    }

    public void setVk(String vk) {
        this.vk = vk;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FriendTypeEnum getType() {
        return type;
    }

    public void setType(FriendTypeEnum type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
