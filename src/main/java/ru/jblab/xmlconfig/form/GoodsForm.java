package ru.jblab.xmlconfig.form;

public class GoodsForm {

    private String category;
    private String nameOfProduct;
    private String countGoods;
    private String typeCount;
    private String price;
    private String comment;
    private Long id;
    private Long shopListId;



    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public String getCountGoods() {
        return countGoods;
    }

    public void setCountGoods(String countGoods) {
        this.countGoods = countGoods;
    }

    public String getTypeCount() {
        return typeCount;
    }

    public void setTypeCount(String typeCount) {
        this.typeCount = typeCount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopListId() {
        return shopListId;
    }

    public void setShopListId(Long shopListId) {
        this.shopListId = shopListId;
    }
}
