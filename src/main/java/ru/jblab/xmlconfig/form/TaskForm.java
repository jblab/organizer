package ru.jblab.xmlconfig.form;

/**
 * Created by root on 10.10.14.
 */
public class TaskForm {
    private String taskname;
    private String priority;
    private String comment;
    private String deadlineDate;
    private String deadlineTime;
    private String date;
    private String status;
    private int redacted;
    private long id;

    public TaskForm() {
    }

    public int getRedacted() {
        return redacted;
    }

    public void setRedacted(int redacted) {
        this.redacted = redacted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(String deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public String getDeadlineTime() {
        return deadlineTime;
    }

    public void setDeadlineTime(String deadlineTime) {
        this.deadlineTime = deadlineTime;
    }
}
