package ru.jblab.xmlconfig.form;

import ru.jblab.xmlconfig.model.enums.EventCategory;

/**
 * Created by tommoto on 10.10.14.
 */
public class EvForm {
    private String name;
    private String date;
    private String price;
    private String description;
    private EventCategory category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Long id;

    public EvForm() {
    }

    public EvForm(Long id, String name, String date, String time,
                  String description, EventCategory category) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.description = description;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventCategory getCategory() {
        return category;
    }

    public void setCategory(EventCategory category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
