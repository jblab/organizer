package ru.jblab.xmlconfig.form;

/**
 * Created by Arcoo on 10.10.2014.
 */
public class KindOfSportForm {

    private Long id;

    private String date;
    private String name;
    private String time;
    private String metric;
    private String sportsKindEnum;

    public String getSportsKindEnum() {
        return sportsKindEnum;
    }

    public void setSportsKindEnum(String sportsKindEnum) {
        this.sportsKindEnum = sportsKindEnum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
