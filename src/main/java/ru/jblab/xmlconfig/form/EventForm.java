package ru.jblab.xmlconfig.form;

/**
 * Created by Dilya on 09.10.2014.
 */
public class EventForm {
    private String eventName;
    private String weekday;
    private String time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Long id;

    public EventForm() {

    }

    public EventForm(String eventName, String weekday, String time) {
        this.eventName = eventName;
        this.weekday = weekday;
        this.time = time;

    }

    public String getEventName() {
        return eventName;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public void setEventName(String name) {
        this.eventName = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    @Override
    public String toString() {
        return "Event{" +
                "eventName='" + eventName + '\'' +
                ", weekday='" + weekday + '\'' +
                ", time=" + time +
                '}';
    }
}