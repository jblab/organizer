package ru.jblab.xmlconfig.utils;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.jblab.xmlconfig.model.User;

/**
 * Created by Александр on 05.11.14.
 */

public class AuthenticationUtil {

    public static User getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken)
            return (User) authentication.getPrincipal();
        else
            return null;

    }
}
