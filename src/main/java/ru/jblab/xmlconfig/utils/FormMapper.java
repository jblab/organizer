package ru.jblab.xmlconfig.utils;


import ru.jblab.xmlconfig.form.*;
import ru.jblab.xmlconfig.model.*;
import ru.jblab.xmlconfig.model.enums.KindOfSportEnum;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Rigen on 08.10.14.
 */
public class FormMapper {

    /**
     * Convert goods model to goods form
     *
     * @param goods
     * @return node
     */
    public static GoodsForm goodsToGodsForm(Goods goods){
        GoodsForm node = new GoodsForm();
        node.setCategory(goods.getCategory());
        node.setNameOfProduct(goods.getNameOfProduct());
        node.setCountGoods(String.valueOf(goods.getCountGoods()));
        node.setTypeCount(goods.getTypeCount());
        node.setPrice(String.valueOf(goods.getPrice()));
        node.setComment(goods.getComment());
        node.setId(goods.getId());
        node.setShopListId(goods.getShopListFK().getId());
        return node;

    }

    /**
     * Convert shop goods form to goods model
     *
     * @param goodsForm
     * @return node
     */
    public static Goods goodsFormToGoods(GoodsForm goodsForm){
        Goods node = new Goods();
        node.setCategory(goodsForm.getCategory());
        node.setNameOfProduct(goodsForm.getNameOfProduct());
        node.setCountGoods(Integer.valueOf(goodsForm.getCountGoods()));
        node.setTypeCount(goodsForm.getTypeCount());
        node.setPrice(Integer.valueOf(goodsForm.getPrice()));
        node.setComment(goodsForm.getComment());
        node.setId(goodsForm.getId());
        return node;
    }
    /**
     * Convert diary's Note form to Note model
     *
     * @param noteForm
     * @return note
     */
    public static NoteModel noteFormToNote(NoteForm noteForm) throws ParseException {
        NoteModel note = new NoteModel();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        note.setNoteKey(new NoteCompositeKey(noteForm.getUser(), sdf.parse(noteForm.getDate()).getTime()));
        note.setNoteText(noteForm.getNoteText());
        return note;
    }

    /**
     * Convert diary's Note model to Note form
     *
     * @param note
     * @return noteForm
     */
    public static NoteForm noteToNoteForm(NoteModel note) {
        NoteForm noteForm = new NoteForm();
        Date date = new Date(note.getNoteKey().getDate());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        noteForm.setDate(sdf.format(date));
        noteForm.setNoteText(note.getNoteText());
        noteForm.setUser(note.getNoteKey().getUser());
        return noteForm;
    }

    // Converts KindOfSport form to kindOfSport model;
    public static KindOfSport
    kindOfSportFormToKindOfSport(KindOfSportForm kindOfSportForm) throws ParseException {
        KindOfSport kindOfSport = new KindOfSport();

//        if (kindOfSportForm.getDate() != null) {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            kindOfSport.setDate(sdf.parse(kindOfSportForm.getDate()).getTime());
//        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        kindOfSport.setDate(sdf.parse(kindOfSportForm.getDate()).getTime());

        kindOfSport.setId(kindOfSportForm.getId());
        kindOfSport.setName(kindOfSportForm.getName());
        kindOfSport.setTime(Double.parseDouble(kindOfSportForm.getTime()));
        kindOfSport.setMetric(kindOfSportForm.getMetric());
        kindOfSport.setSportsKindEnum(KindOfSportEnum.getByValue(kindOfSportForm.getSportsKindEnum()));  //!!!!!!!!!!!!!!!!!!!!!!!
        return kindOfSport;
    }

    // Converts KindOfSport model to kindOfSport form;
    public static KindOfSportForm kindOfSportToKindOfSportForm(KindOfSport kindOfSport) {
        KindOfSportForm kindOfSportForm = new KindOfSportForm();

        Date date = new Date(kindOfSport.getDate());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        kindOfSportForm.setDate(sdf.format(date));

        kindOfSportForm.setId(kindOfSport.getId());
        kindOfSportForm.setName(kindOfSport.getName());
        kindOfSportForm.setTime(Double.toString(kindOfSport.getTime()));
        kindOfSportForm.setMetric(kindOfSport.getMetric());
        kindOfSportForm.setSportsKindEnum(kindOfSport.getSportsKindEnum().getValue());  //!!!!!!!!!!!!!!!!!!!!!!!
        return kindOfSportForm;
    }

    /**
     * Friend Form to Friend model
     *
     * @param friendForm
     * @return
     */
    public static Friend friendFromToFriend(FriendForm friendForm) {
        Friend friend = new Friend();
        friend.setId(friendForm.getId());
        friend.setName(friendForm.getName());
        friend.setType(friendForm.getType());
        friend.setBdate(friendForm.getBdate());
        friend.setEmail(friendForm.getEmail());
        friend.setFacebook(friendForm.getFacebook());
        friend.setInstagram(friendForm.getInstagram());
        friend.setSkype(friendForm.getSkype());
        friend.setTelephone(friendForm.getTelephone());
        friend.setVk(friendForm.getVk());
        return friend;
    }

    /**
     * Friend model to Friend From
     */
    public static FriendForm friendToFriendForm(Friend friend) {
        FriendForm friendForm = new FriendForm();
        friendForm.setId(friend.getId());
        friendForm.setName(friend.getName());
        friendForm.setType(friend.getType());
        friendForm.setBdate(friend.getBdate());
        friendForm.setEmail(friend.getEmail());
        friendForm.setFacebook(friend.getFacebook());
        friendForm.setInstagram(friend.getInstagram());
        friendForm.setSkype(friend.getSkype());
        friendForm.setTelephone(friend.getTelephone());
        friendForm.setVk(friend.getVk());
        return friendForm;
    }


    public static EvForm evToEvForm(Ev event) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
        EvForm evForm = new EvForm();
        evForm.setId(event.getId());
        evForm.setName(event.getName());
        evForm.setDate(sdf.format(event.getDate()));
        evForm.setPrice(event.getPrice());
        evForm.setCategory(event.getCategory());
        evForm.setDescription(event.getDescription());
        return (evForm);
    }

    public static Ev evFormToEv(EvForm evForm) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
        Ev event = new Ev();
        event.setId(evForm.getId());
        event.setName(evForm.getName());
        Long date = sdf.parse(evForm.getDate()).getTime();
        event.setDate(date);
        event.setPrice(evForm.getPrice());
        event.setCategory(evForm.getCategory());
        event.setDescription(evForm.getDescription());
        return event;
    }

    @SuppressWarnings("deprecation")
    public static Event eventFormToEvent(EventForm eventForm) {
        Event event = new Event();
        event.setWeekday(eventForm.getWeekday());
        event.setEventName(eventForm.getEventName());
        event.setTime(new Time(Integer.parseInt(eventForm.getTime().substring(0, 2)), Integer.parseInt(eventForm.getTime().substring(3, 5)), 00));
       event.setId(eventForm.getId());
        return event;
    }

    public static EventForm eventToEventForm(Event event) {
        EventForm eventForm = new EventForm();
        eventForm.setTime(""+event.getTime().getHours()+':'+event.getTime().getMinutes());
        eventForm.setEventName(event.getEventName());
        eventForm.setWeekday(event.getWeekday());
        eventForm.setId(event.getId());
        return eventForm;
    }

    /**
     * Convert taskForm to Task
     *
     * @param taskForm - form to convert
     * @return converted form
     */
    public static Task taskFormToTask(TaskForm taskForm) {
        Task task = new Task();
        task.setTaskname(taskForm.getTaskname());
        task.setComment(taskForm.getComment());
        task.setPriority(taskForm.getPriority());
        try {
            if (!taskForm.getDeadlineDate().equals("") && !taskForm.getDeadlineTime().equals("")) {
                task.setDeadline(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(taskForm.getDeadlineDate() + " " + taskForm.getDeadlineTime()));
            } else if (!taskForm.getDeadlineDate().equals("")) {
                task.setDeadline(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(taskForm.getDeadlineDate() + " 12:00"));
            }
            if (taskForm.getDate() != null) {
                task.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(taskForm.getDate()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (taskForm.getId() != 0L) {
            task.setId(taskForm.getId());
        }
        task.setStatus(Integer.parseInt(taskForm.getStatus()));
        if (taskForm.getRedacted() == 1) {
            task.setRedacted(1);
        } else {
            task.setRedacted(0);
        }
        return task;
    }

    public static TaskForm taskToTaskForm(Task task) {
        TaskForm taskForm = new TaskForm();
        taskForm.setTaskname(task.getTaskname());
        taskForm.setComment(task.getComment());
        taskForm.setPriority(task.getPriority());
        if (task.getDeadline() != null) {
            String deadline = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(task.getDeadline());
            taskForm.setDeadlineDate(deadline.substring(0, 10));
            taskForm.setDeadlineTime(deadline.substring(11, deadline.length()));
        }
        taskForm.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(task.getDate()));
        taskForm.setId(task.getId());
        taskForm.setStatus(String.valueOf(task.getStatus()));
        if (task.getRedacted() == 1) {
            taskForm.setRedacted(1);
        } else {
            taskForm.setRedacted(0);
        }
        return taskForm;
    }

    // Converts User form to User model;
    public static User userFormToUser(UserForm userForm) {

        User user = new User();
        user.setId(userForm.getId());
        user.setName(userForm.getName());
        user.setPassword(userForm.getPassword());
        user.setSalt(userForm.getSalt());
        return user;

    }

    // Converts User model to User form;
    public static UserForm UserToUserForm(User user) {

        UserForm userForm = new UserForm();
        userForm.setId(user.getId());
        userForm.setName(user.getName());
        userForm.setPassword(user.getPassword());
        userForm.setSalt(user.getSalt());
        return userForm;

    }

}