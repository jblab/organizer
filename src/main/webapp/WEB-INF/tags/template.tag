<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@tag description="Simple Template" pageEncoding="UTF-8" %>

<%@attribute name="title" %>
<%@attribute name="head" fragment="true" %>
<%@attribute name="body" fragment="true" required="true" %>

<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>

    <jsp:invoke fragment="head"/>
</head>
<body>
<div class="container">

    <div id="login_logout">
        <table>
            <security:authorize access="isAuthenticated()">
                <h4> User: <security:authentication property="principal.name"/></h4>
                <tr>
                    <td><a href="/logout" class="addButton"> Logout </a></td>
                </tr>
            </security:authorize>
            <security:authorize access="isAnonymous()">
                <h4> User: <security:authentication property="principal"/></h4>
                <tr>
                    <td><a href="/login" class="addButton"> Login </a></td>
                </tr>
            </security:authorize>
        </table>
    </div>
    <jsp:invoke fragment="body"/>
</div>
</body>
</html>

