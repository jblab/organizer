<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Arcoo
  Date: 11.10.2014
  Time: 23:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit sportResults</title>
    <link href="/resources/css/sportRes.css" rel="stylesheet">
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/sportResScript.js"></script>
</head>
<body>
<h2>Here you can edit your sport result</h2>
<form:form class="addForm" method="POST" action="/sportResults/editSportRes/${kindOfSportToEdit.id}"
           modelAttribute="kindOfSportToEdit">

    Please edit <b>${kindOfSportToEdit.name}</b> data:
    <form:input path="name" class="js-kindOfSportName" maxlength="20" placeholder="enter kind of sport"/><br>
    <form:errors class="error" path="name"/><br>

    <form:hidden path="date" class=".js-kindOfSportDate" value='${kindOfsportToEdit.dateStr}'></form:hidden>

    <form:select path="sportsKindEnum" class="js-sportsKindEnum">
        <%--<option selected value="${defaultSportKindsEnum.value}"></option>--%>
        <c:forEach items="${sportKindsEnumList}" var="sportsKind">
            <form:option value="${sportsKind}">${sportsKind.value}</form:option>
        </c:forEach>
    </form:select><br>
    <%--<form:errors class="error" path="sportsKindEnum"/><br>--%>

    <br><form:input path="time" class="js-time" maxlength="10" placeholder="enter time of exercise"/><br>
    <form:errors class="error" path="time"/><br>

    <form:select path="metric" class="js-metric">
        <form:option value="" label="Select"/>
        <form:option value="minutes" label="min"/>
        <form:option value="seconds" label="sec"/>
        <form:option value="hour(s)" label="hour(s)"/>
        <form:option value="kilometres" label="km"/>
        <form:option value="metres" label="metres"/>
        <form:option value="repeats" label="repeats"/>
    </form:select><br>
    <form:errors class="error" path="metric"/><br>

    <input type="submit" value="Edit" class="addButton"/>
    <input type="hidden" name="id" class="js-id" value='${kindOfSportToEdit.id}'>

    <%--<button type="submit" id="edit_btn" class="addButton"> Edit </button>--%>
    <a href="javascript:history.back()" title="Back" class="addButton"> Back </a>
</form:form>
</body>
</html>