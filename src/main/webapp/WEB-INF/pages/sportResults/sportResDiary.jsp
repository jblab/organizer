<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--
  Created by IntelliJ IDEA.
  User: ArthurRec = Arcoo
  Date: 11.11.2014
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <title>SportRes diary</title>
    <link href="/resources/css/sportRes.css" rel="stylesheet">
    <link href="/resources/css/sportResInline.css" rel="stylesheet">
    <link type="text/css" href="/resources/css/jquery-ui.min.css" rel="stylesheet"/>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/jquery-ui.min.js"></script>
    <script src="/resources/js/sportResScript.js"></script>
    <meta http-equiv="Content-Type" Content="text/html; charset=UTF-8">
</head>

<body>
<h2 align='center'> Sport results diary </h2>

<div id="datepicker"></div>
<br>

<div id="addSpecSportResListInclude" class="addForm">
    <%--<c:if test="${empty listOfSports}">
      <h3>You haven't got any sport results</h3>

      <div id="addSportResJspInclude"/>

    </c:if>
    <c:if test="${not empty listOfSports}">
      <div cssClass="addForm">
        <c:forEach items="${listOfSports}" var="kindOfsport">

          <div id="addSportResJspInclude">
              <%--<c:set var="kindOfsport" value="${kindOfsport}" scope="request"/>
            <jsp:include page="addsportresInclude.jsp"/>
          </div>

        </c:forEach><br>
        <a href="/sportResults/clearSportRes" class="addClearButton">clearList</a>
      </div>
    </c:if>--%>
    <jsp:include page="addSpecSportResListInclude.jsp"/>
</div>
<br>

<div class="addForm">
    <form action="/sportResults/sportRes" method="get">
        <input type="submit" value="Add result" class="addButton">
    </form>
    <button type="submit" id="clear_list" class="addClearButton"> clearList</button>
    <br>
</div>
<br>
</body>
</html>
