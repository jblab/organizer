<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--
  Created by IntelliJ IDEA.
  User: ArthurRec
  Date: 11.11.2014
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>

<div>
    <c:if test="${empty specificListOfSports}">
        <h3>You haven't got any sport results</h3>
    </c:if>

    <c:if test="${not empty specificListOfSports}">
        <div>
            <c:forEach items="${specificListOfSports}" var="kindOfsport">

                <span style="font-size: larger; ">
                ${kindOfsport.dateStr}<br>
                <b>${kindOfsport.sportsKindEnum.value}</b><br>
                   ${kindOfsport.name}
                   ${kindOfsport.time}
                   ${kindOfsport.metric}
                </span>
                <br>

                <%--<input type="hidden" name="id" class="js-id" value='${kindOfsport.id}'/>--%>
                <%--<a href="/sportResults/delete/${kindOfsport.id}" class="addButton"> Delete </a>--%>

                <a href="/sportResults/edit/${kindOfsport.id}" class="addButton"> Edit </a>
                <button type="submit" class="js-delete_result addButton"
                        name="id" value='${kindOfsport.id}'> Delete
                </button>
                <br><br>

            </c:forEach>
        </div>
    </c:if>
    <%--<form:hidden path="secretValue" />--%>
    <%--<a href="/sportResults/clearSportRes" class="addClearButton">clearList</a>--%>
    <%--<button type="submit" id="clear_list" class="addClearButton">clearList</button><br>--%>
    <%--name="dateStr" value='${kindOfsport.dateStr}'--%>
</div>