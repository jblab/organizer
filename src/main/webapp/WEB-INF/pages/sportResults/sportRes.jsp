<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My sport results</title>
    <link href="/resources/css/sportRes.css" rel="stylesheet">
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/sportResScript.js"></script>
</head>

<body>
<form:form class="addForm" action="/sportResults/addSportRes" method="post" modelAttribute="kindOfSport">
    <form:input path="name" class="js-kindOfSportName"  maxlength="20" placeholder="enter kind of sport"/><br>
    <form:errors class="error" path="name"/><br>

    <form:input type="date" path="date" class=".js-kindOfSportDate" placeholder="enter date of exercise"/><br>
    <%--<form:errors class="error" path="date"/><br>--%><br>

    <form:select path="sportsKindEnum" class="js-sportsKindEnum">
        <%--<option selected value="${defaultSportKindsEnum.value}">sportsKind</option>--%>
        <c:forEach items="${sportKindsEnumList}" var="sportsKind">
            <form:option value="${sportsKind}">${sportsKind.value}</form:option>
        </c:forEach>
    </form:select><br>

    <br><form:input path="time" class="js-time" maxlength="10" placeholder="enter time of exercise"/><br>
    <form:errors class="error" path="time"/><br>

    <form:select path="metric" class="js-metric">
        <form:option value="" label="Select"/>
        <form:option value="minutes" label="min"/>
        <form:option value="seconds" label="sec"/>
        <form:option value="hour(s)" label="hour(s)"/>
        <form:option value="kilometres" label="km"/>
        <form:option value="metres" label="metres"/>
        <form:option value="repeats" label="repeats"/>
    </form:select>

    <%--<button type="submit" id="add_btn" class="addButton">Add value JS</button><br>--%>
    <input type="submit" value="Add" class="addButton"/><br>
    <form:errors class="error" path="metric"/><br>

    <a href="javascript:history.back()" title="Back" class="addButton"> Back </a>
    <a href="/" title="HomePage" class="addButton"> HomePage </a><br>
</form:form>

<%--<div>
    <c:if test="${empty listOfSports}">
        <h3>You haven't got any sport results</h3>

        <div id="addSportResJspInclude"/>

    </c:if>
<c:if test="${not empty listOfSports}">
    <form:form cssClass="addForm">
        <c:forEach items="${listOfSports}" var="kindOfsport">

            <div id="addSportResJspInclude">
                <%--<c:set var="kindOfsport" value="${kindOfsport}" scope="request"/>
                <jsp:include page="addsportresInclude.jsp"/>
            </div>

        </c:forEach><br>
        <a href="/sportResults/clearSportRes" class="addClearButton">clearList</a>
    </form:form>
</c:if>
</div>--%>

<div class="addForm">
    <a href="/sportResults/sportResDiary" class="addButton"> SportRes diary </a>
    <a href="/" title="HomePage" class="addButton"> HomePage </a>
</div>

</body>
</html>