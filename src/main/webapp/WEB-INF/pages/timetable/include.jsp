<%--
  Created by IntelliJ IDEA.
  User: A
  Date: 31.10.2014
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${empty events}">

        Нет событий

</c:if>
<c:if test="${not empty events}">
    <c:forEach items="${events}" var="result">
        <a href="/timetable/edit/${result.id}">${result.eventName}</a>
        ${result.time}<br>
    </c:forEach>

</c:if>
<br>