<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: A
  Date: 07.10.2014
  Time: 23:50
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link href="../resources/css/event.css" rel="stylesheet">
    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/timetablescript.js"></script>

    <title>
        My timetable
    </title>
</head>
<body>

<div class="tasks">
    <br><br>
<input type="button" value="<--" id="js-left1">
<span id="js-day">Monday</span>
<input type="button" value="-->" id="js-right1">
<br><br>
<div class="js-render">
    <jsp:include page="include.jsp">
        <jsp:param name="events" value='${events}'/>
    </jsp:include>
</div>
<form action="/timetable/create" method="get" >
    <input type="submit" value="+"  class="myButton btn btn-primary">
</form>
</div>
</body>
</html>
