<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: A
  Date: 07.10.2014
  Time: 23:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link href="../resources/css/event.css" rel="stylesheet">
    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/timetablescript.js"></script>
    <title>Create an event</title>
</head>
<body>
<div class="create">
<h1> Создание нового события</h1>

<form:form action="/timetable/add" method="post" modelAttribute="event">
    <h4>Введите название события:</h4>
    <form:input type="text" class="js-eventName" path="eventName"/>
    <form:errors path="eventName" cssClass="error"/>

    <h4>Введите время события:</h4>

    <form:input type="time" path="time" cssClass="js-time" />
    <form:errors path="time" cssClass="error"/>
    <br>

 <h4>Выберите день недели:</h4><br>
    <input type="button" value="<--" id="js-left">
    <form:hidden path="weekday" id="js-week" value="Monday"></form:hidden>
    <span id="js-day">Monday</span>
    <input type="button" value="-->" id="js-right">
    <br><br>
        <form:errors path="weekday" cssClass="error"/><br><br>
    <input type="submit" value="Сохранить" class="btn btn-primary"><br><br>
    <button type="submit" id="add_btn" class="addButton">Add by JS</button>
</form:form>
    <form action="/timetable/show" method="get">
        <button type='submit' class="btn btn-success">Вернуться</button>
    </form>

    </div>
</body>
</html>