<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class = "js-list-element">
    ${param.name}<br/>
    ${param.fdate}<br/>
    <c:if test="${not empty param.descriptionPreview}">
        ${param.descriptionPreview}
    </c:if>
    <a href="/events/event${param.id}">...</a><br/>
    <hr>
</div>