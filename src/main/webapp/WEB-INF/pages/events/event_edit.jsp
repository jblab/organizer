<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sform" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:template title="Внесение изменений">
    <jsp:attribute name="head">

        <title>Editing an Event</title>
        <script type="text/javascript" src="/resources/bootstrap-timepicker/jquery-2.1.1.js"></script>
                <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/resources/bootstrap-timepicker/moment.js"></script>
        <script type="text/javascript" src="/resources/bootstrap-timepicker/transition.js"></script>
        <script type="text/javascript" src="/resources/bootstrap-timepicker/collapse.js"></script>
        <script type="text/javascript" src="/resources/bootstrap-timepicker/bootstrap-datetimepicker.js"></script>
        <link rel="stylesheet" href="/resources/bootstrap-timepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
</jsp:attribute>
<jsp:attribute name="body">
    <c:set var="category" value="${eventEnums}" scope="request"/>
    <jsp:include page="category_navigation_include.jsp" ></jsp:include>
    <sform:form id="form" action="/events/submitEdit?id=${id}" method="POST"  modelAttribute="eventForm">
        <sform:label path="name">Name</sform:label>
        <sform:input id="js-name" placeholder="Enter event name.." path="name"/>
        <sform:errors path="name"/>

        <br>
        <%--TODO disable input field--%>
        <sform:label path="date">Date & Time</sform:label>
        <div class="row">
            <div class='col-sm-6'>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker2'>
                        <sform:input id="js-date" type='text' class="form-control"  path="date"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker2').datetimepicker({
//                        language: 'ru' не всегда работает, нет приоритета с ним сегодня бороться
                    });
                });
            </script>
        </div>
        <sform:errors path="date"/>

        <br>
        <sform:label path="price">Price</sform:label>
        <sform:input id="js-price" placeholder="price1 - price2" path="price"/>
        <sform:errors path="price"/>

        <br>
        <sform:label path="description">Description</sform:label>
        <sform:input path="description"/>

        <br>
        <sform:label path="category">Category</sform:label>
        <sform:select path="category">
            <c:forEach var="category" items="${categories}">
                <sform:option value="${category}">${category.value}
                </sform:option>
            </c:forEach>
        </sform:select>

        <br>
        <input type="submit" id="submitButton" value="Submit"/>
    </sform:form>

    <c:out value="${id}"></c:out>
</jsp:attribute>
</t:template>