<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${event.name}</title>
</head>
<body>
<c:set var="category" value="${eventEnums}" scope="request"/>
<jsp:include page="category_navigation_include.jsp" ></jsp:include>
<h2>Event ${event.name}'s info</h2>

<form action='/events' method="get">
    <button type='submit' >Home</button>
</form>
<table>
    <tr>
        <td>Name</td>
        <td>${event.name}</td>
    </tr>
    <tr>
        <td>Date</td>
        <td>${event.fdate}</td>
    </tr>
    <tr>
        <td>Price</td>
        <td>${event.price}</td>
    </tr>
    <tr>
        <td>Description</td>
        <td>${event.description}</td>
    </tr>
    <tr>
        <td>Category</td>
        <td>${event.category}</td>
    </tr>
</table>
<form action='/events/deleteEvent' method="post">
    <button type='submit' name="id" value='${event.id}'>Delete</button>
</form>
<form action='/events/editEvent' method="get">
    <button type='submit' name="id" value='${event.id}'>Edit</button>
</form>
</body>
</html>