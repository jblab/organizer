<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


  <br/>
  <c:forEach var = "eventCategory" items = "${requestScope.category}">
    <a href="/events/cat/${eventCategory}">${eventCategory}</a>
    <br/>
  </c:forEach>
  <br/>
