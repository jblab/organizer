<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="js/jquery-ui-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../resources/css/calendar.css">

<script src="/resources/js/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/events.js"></script>
<link href="/resources/css/bootstrap.min.css" rel="stylesheet">
<%--calendar--%>
<t:template title="sdfdsf">
    <jsp:attribute name="head">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="js/jquery-ui-datepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../resources/css/calendar.css">
        <script src="/resources/js/jquery.min.js"></script>
        <script src="/resources/js/bootstrap.min.js"></script>
        <script src="/resources/js/events.js"></script>
        <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    </jsp:attribute>
<jsp:attribute name="body">
<a href="events/newEvent">Add one</a>
    <%--<c:out value="${startPage}"></c:out>
    <c:out value="${endPage}"></c:out>
    <c:out value="${pageNum}"></c:out>--%>
<c:forEach begin="${startPage}" end="${endPage}" var="curPage">
    <c:if test="${curPage ne pageNum && curPage != 0}"><a href="/events?page=${curPage}">${curPage} </a></c:if>
    <c:if test="${curPage eq pageNum}"><p> ${curPage} </p></c:if>
</c:forEach>
<c:if test="${pageNum ne endPage && pageNum > 1}">
    <a href="/events?page=${pageNum + 1}">Next</a>
</c:if>

<h3>Upcoming events: </h3>
<c:out value="${eventEnums}"></c:out>
<c:set var="category" value="${eventEnums}" scope="request"/>
<jsp:include page="category_navigation_include.jsp"></jsp:include>

<br/>
<c:if test="${empty eventsList}">
    No events yet!
</c:if>
<c:if test="${not empty eventsList}">
    <c:forEach var = "event" items="${eventsList}">
        <jsp:include page="event_info_unclude.jsp">
            <jsp:param name="name" value="${event.name}" />
            <jsp:param name="id" value="${event.id}" />
            <jsp:param name="fdate" value="${event.fdate}" />
            <jsp:param name="descriptionPreview" value="${event.descriptionPreview}" />
        </jsp:include>
    </c:forEach>
</c:if>

<c:forEach var = "eventCategory" items = "${eventEnums}">
    <div class="tab-pane fade" id="${eventCategory}">
        <c:if test="${not empty eventsList}">
            <c:forEach  var="event" items="${eventsList}">
                <c:if test="${event.category.value == eventCategory}">
                    <jsp:include page="event_info_unclude.jsp">
                        <jsp:param name="id" value="${event.id}" />
                        <jsp:param name="fdate" value="${event.fdate}" />
                        <jsp:param name="name" value="${event.name}" />
                        <jsp:param name="descriptionPreview" value="${event.descriptionPreview}" />
                    </jsp:include>
                </c:if>
            </c:forEach>
        </c:if>
    </div>
</c:forEach>

</jsp:attribute>
</t:template>