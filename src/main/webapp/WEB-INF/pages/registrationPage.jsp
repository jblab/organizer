<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>--%>
<%--
  Created by IntelliJ IDEA.
  User: Arcoo
  Date: 07.11.2014
  Time: 0:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Registration page</title>
    <link href="/resources/css/sportRes.css" rel="stylesheet">
    <script src="/resources/js/jquery.min.js"></script>
    <%--<script src="/resources/js/sportResScript.js"></script>--%>
</head>
<body>
<h1>Регистрация:</h1>
<form:form class="addForm" method="POST" action="/addUser" modelAttribute="UserForm">

    <h4>Введите имя пользователя:</h4>
    <form:input path="name" class="js-username" maxlength="20" placeholder="введите имя"/><br>
    <form:errors class="error" path="name"/><br>

    <h4>Введите пароль:</h4>
    <br><form:input type="password" path="password" class="js-password" maxlength="10"
                    placeholder="введите пароль"/><br>
    <form:errors class="error" path="password"/><br>

    <h4>Введите повторно пароль:</h4>
    <br><form:input type="password" path="confirmPassword" class="js-password" maxlength="10"
                    placeholder="введите повторно пароль"/><br>
    <form:errors class="error" path="confirmPassword"/><br>

    <input type="submit" value="sign up" class="addButton"/><br>

    <%--<button type="submit" id="add_btn" class="addButton">Add value JS</button>--%>
    <%--<a href="javascript:history.back()" title="Back" class="addButton"> Back </a>--%>
    <%--<a href="/" title="HomePage" class="addButton"> HomePage </a><br>--%>
</form:form>

</body>
</html>
