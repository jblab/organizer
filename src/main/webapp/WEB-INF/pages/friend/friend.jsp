<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 07.10.14
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:template title="Home Friend">
    <jsp:attribute name="head">
        <script src="../../resources/js/jquery.min.js"></script>
        <link rel="stylesheet" href="../../resources/css/datepicker.css">
        <script src="../../resources/js/bootstrap-datepicker.js"></script>
        <%--<script src="../../resources/js/bootstrap.min.js"></script>--%>
        <script src="../../resources/js/friend.js"></script>
        <link href="../../resources/css/bootstrap.min.css" type="text/css"/>
        <style>
            <%@include file="/WEB-INF/resources/css/bootstrap.min.css" %>
        </style>
    </jsp:attribute>
    <jsp:attribute name="body">
        <div class="row">
            <div class="col-md-6">
                <img src="../../resources/img/7-512.png">
            </div>
            <div class="col-md-6">
                <form:form id="friendForm" method="post" action="/add/friend" modelAttribute="friendForm">
                    <table>
                        <tr>
                            <th>Group:</th>
                            <th>
                                <form:select style="width: 100%" id="type" path="type">
                                    <c:forEach var="friendType" items="${friendEnum}">
                                        <option value="${friendType}">${friendType.value}</option>
                                    </c:forEach>
                                </form:select>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>*FIO</th>
                            <th>

                                <form:input id="name" style="width: 100%" path="name"/>
                            </th>
                            <th><form:errors path="name"/></th>
                        </tr>
                        <tr style="height: 26px">
                            <th>Telephone</th>
                            <th>
                                <form:input id="telephone" style="width: 100%" path="telephone"/>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>E-mail</th>
                            <th>
                                <form:input id="email" style="width: 100%" path="email"/>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>BDate</th>
                            <th>
                                <form:input path="bdate" type="text" class="span2" value="02/16/12"
                                            data-date-format="mm/dd/yy" id="bdate"/>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>Vk(id)</th>
                            <th>
                                <form:input id="vk" style="width: 100%" path="vk"/>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>Instagram</th>
                            <th>
                                <form:input id="instagram" style="width: 100%" path="instagram"/>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>Skype</th>
                            <th>
                                <form:input id="skype" style="width: 100%" path="skype"/>
                            </th>
                        </tr>
                        <tr style="height: 26px">
                            <th>Facebook</th>
                            <th>
                                <form:input id="facebook" style="width: 100%" path="facebook"/>
                            </th>
                        </tr>
                    </table>
                    <input type="submit" value="Ok">
                </form:form>
            </div>
        </div>
    </jsp:attribute>
</t:template>
<%--<html>--%>
<%--<head>--%>
<%--<title>My Friend</title>--%>
<%--<script src="../../resources/js/jquery.min.js"></script>--%>
<%--<script src="../../resources/js/bootstrap.min.js"></script>--%>
<%--<script src="../../resources/js/friend.js"></script>--%>


<%--<link href="../../resources/css/bootstrap.min.css" type="text/css"/>--%>
<%--<style >--%>
<%--<%@include file="/WEB-INF/resources/css/bootstrap.min.css" %>--%>
<%--</style>--%>
<%--</head>--%>
<%--<body>--%>
<%--<c:if test="${not empty error}">--%>
<%--${error}<br>--%>
<%--</c:if>--%>
<%--<form:form id="friendForm" action="/addFriends" modelAttribute="friendForm" method="post">--%>
<%--<form:errors path="name"/>--%>
<%--<form:input id="name" path="name" placeholder="Enter name your Friend"/>--%>
<%--<form:select id="type" path="type">--%>
<%--<c:forEach var="friendType" items="${friendEnum}">--%>
<%--<form:option value="${friendType.value}">${friendType.value}</form:option>--%>
<%--</c:forEach>--%>
<%--</form:select>--%>
<%--<button type="submit" value="Add" class="button">ff</button>--%>
<%--</form:form>--%>


<%--<ul id="myTab" class="nav nav-tabs">--%>
<%--<li><a href="#friend" data-toggle="tab">It's your Friend</a></li>--%>
<%--<li><a href="#schoolmate" data-toggle="tab">It's your schoolmate</a></li>--%>
<%--<li><a href="#family" data-toggle="tab">It's your family</a></li>--%>
<%--<li><a href="#familiar" data-toggle="tab">It's your familiar</a></li>--%>
<%--</ul>--%>
<%--<div id="myTabContent" class="tab-content">--%>
<%--<c:forEach var="friendType" items="${friendEnum}">--%>
<%--<div class="tab-pane fade" id="${friendType.value}">--%>
<%--<c:if test="${empty friendMap.get(friendType.value)}">--%>
<%--You have no ${friendType.value} :(--%>
<%--</c:if>--%>
<%--<c:if test="${not empty friendMap.get(friendType.value)}">--%>
<%--<table border="1" style="width: 80%; margin: auto">--%>
<%--<tr>--%>
<%--<th>Name</th>--%>
<%--<th>Type</th>--%>
<%--<th>Set type</th>--%>
<%--<th>Delete</th>--%>
<%--</tr>--%>
<%--<c:forEach items="${friendMap.get(friendType.value)}" var="fM">--%>
<%--<tr>--%>
<%--<td>${fM.name}</td>--%>
<%--<td>${fM.type}</td>--%>
<%--<td>--%>
<%--<form action="/setTypeFriend?id=${fM.id}" method="post">--%>
<%--<select name="menu" size="1">--%>
<%--<c:forEach var="friendType1" items="${friendEnum}">--%>
<%--<option value="${friendType1.value}">${friendType1.value}</option>--%>
<%--</c:forEach>--%>
<%--</select>--%>
<%--<button type="submit" value="Set">Set</button>--%>
<%--</form>--%>
<%--</td>--%>
<%--<td>--%>
<%--<form action="/deleteFriend?id=${fM.id}" method="post">--%>
<%--<button type="action">Delete</button>--%>
<%--</form>--%>
<%--</td>--%>
<%--</tr>--%>
<%--</c:forEach>--%>
<%--</table>--%>
<%--</c:if>--%>
<%--</div>--%>
<%--</c:forEach>--%>
<%--</div>--%>
<%--</body>--%>
<%--</html>--%>
