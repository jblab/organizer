<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: admiral
  Date: 02.11.14
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../../resources/css/reset.css">
    <link rel="stylesheet" href="../../resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/shop.css">
    <script type="text/javascript" src="../../resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../resources/js/shopscript.js"></script>
    <title>shop lists</title>
</head>

<body onselectstart="return false" onmousedown="return false">
<div id="content">
    <div>
        <a href="/shop/create-list">
            <button id="add" class="btn btn-danger glyphicon glyphicon-plus"></button>
        </a>
    </div>
    <h1 id="empty-list" class="hidden">Список пуст</h1>

    <c:forEach items="${lists}" var="list">

        <div class="list js-list" id="list-${list.id}">
            <span>${list.name}</span>
            <a href="/shop/list/${list.id}">
                <table class="table table-condensed">
                    <c:set value="0" var="count"/>
                    <c:forEach items="${list.goods}" var="goods">
                        <tr>
                            <c:set var="count" value="${count+1}"/>
                            <td> ${count}</td>
                            <td>${goods.nameOfProduct}</td>
                        </tr>

                    </c:forEach>
                </table>
                <h5>Итого: ${summaryPriceMap[list.id]}</h5>
            </a>
            <button id="${list.id}" value="${list.id}"
                    class="glyphicon glyphicon-trash js-del-list del btn-default btn"></button>
        </div>
    </c:forEach>
</div>
</body>
</html>
