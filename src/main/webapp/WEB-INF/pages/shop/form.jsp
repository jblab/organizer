<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: admiral
  Date: 06.11.2014
  Time: 20:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form:form action="/shop/add" class="form" method="post" modelAttribute="goodsForm">
    <form:hidden id="listId" path="shopListId" value="${shopList.id}"/>
    <input type="hidden" value="" id="goods-id"/>
    <form:input type="text" id="category" class="form-control" autofocus="true" placeholder="Категория"
                path="category"/>
    <form:input type="text" id="name" class="form-control" placeholder="Название" path="nameOfProduct"/>
    <form:input type="text" id="count-goods" class="nonString form-control" placeholder="Количество" path="countGoods"/>
    <form:select class="form-control type-count" path="typeCount">
        <form:option value="pcs"/>
        <form:option value="kg"/>
        <form:option value="l"/>
        <form:option value="boxes"/>
        <form:option value="bottle"/>
        <form:option value="milliliters"/>
        <form:option value="g"/>
        <form:option value="packaging"/>
    </form:select>
    <form:input type="text" id="price" class="nonString form-control" placeholder="Цена" path="price"/>
    <form:textarea name="comment" id="comment" class="form-control" rows="2" placeholder="Комментарий"
                   path="comment"/>
    <input type="submit" class="btn btn-success add" id="js-add" value="Добавить">
    <input type="submit" class="btn btn-info update hidden" id="js-update" value="Обновить">
    <input type="button" class="btn btn-danger cancel-update hidden" id="js-cancel" value="Отмена">
</form:form>
