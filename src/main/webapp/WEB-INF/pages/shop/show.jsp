<%--
  Created by IntelliJ IDEA.
  User: admiral
  Date: 05.11.2014
  Time: 16:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: admiral
  Date: 02.11.14
  Time: 18:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../../resources/css/reset.css">
    <link rel="stylesheet" href="../../resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/shop.css">
    <script type="text/javascript" src="../../resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../resources/js/shopscript.js"></script>
    <title>add goods</title>
</head>
<body>
<header>
    <a href="/shop/lists">
        <button type="button" class="btn btn-success">к списку</button>
    </a>
    <jsp:include page="nameList.jsp"/>
</header>
<div id="form">
    <jsp:include page="form.jsp"/>

    <%--TODO доделать это--%>
    <div id="goods-li">
        <table class="table table-hover" id="js-table">
            <tr id="lable">
                <th>Категория</th>
                <th>Название</th>
                <th>Количество</th>
                <th>Цена</th>
                <th>Комментарий</th>
            </tr>
            <c:forEach items="${productList}" var="list">
                <tr id="js-tr-${list.id}">
                    <input type="hidden" value="${list.id}" class="id"/>
                    <td id="categoryTable${list.id}">${list.category}</td>
                    <td id="nameTable${list.id}">${list.nameOfProduct}</td>
                    <td id="countGoodsTable${list.id}" value="${list.countGoods}">${list.countGoods}<label
                            id="typeCountTable${list.id}">${list.typeCount}</label></td>
                    <td class="js-price-sum" id="priceTable${list.id}">${list.price}<label> руб</label></td>
                    <td id="commentTable${list.id}">${list.comment}</td>
                    <td>
                        <button id="${list.id}" value="${list.id}"
                                class="js-edit glyphicon glyphicon-pencil edt btn-default btn"></button>
                    </td>
                    <td>
                        <button id="${list.id}" value="${list.id}"
                                class="glyphicon glyphicon-trash js-del del btn-default btn"></button>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <h4 id="replace-price">Общая стоимость: <label id="sum-price">${fullPrice}</label></h4>
    </div>
</div>
</body>
</html>
