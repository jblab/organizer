<%--
  Created by IntelliJ IDEA.
  User: admiral
  Date: 06.11.14
  Time: 3:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<input type="hidden" value="${goods.id}" class="id"/>
<td id="categoryTable${goods.id}">${goods.category}</td>
<td id="nameTable${goods.id}">${goods.nameOfProduct}</td>
<td id="countGoodsTable${goods.id}" value="${goods.countGoods}">${goods.countGoods}<label id="typeCountTable${goods.id}"> ${goods.typeCount}</label></td>
<td class="js-price-sum" id="priceTable${goods.id}">${goods.price}<label> руб</label></td>
<td id="commentTable${goods.id}">${goods.comment}</td>
<td>
    <button id="${goods.id}" value="${goods.id}" class="js-edit glyphicon glyphicon-pencil edt btn-default btn"></button>
</td>
<td>
    <button id="${goods.id}" value="${goods.id}"
            class="glyphicon glyphicon-trash js-del del btn-default btn"></button>
</td>
