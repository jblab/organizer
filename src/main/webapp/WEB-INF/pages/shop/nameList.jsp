<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: admiral
  Date: 07.11.14
  Time: 2:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form:label class="list-name" path="shopList"><h1>${shopList.name}</h1></form:label>
<input type="hidden" value="${shopList.id}" id="list_id"/>
<button class="btn btn-info edit-namelist" id="edit-name-list">Edit</button>
<input type="text" class="form-control hidden" id="list-name-input" placeholder="Введите имя"/>
<input class="btn btn-info hidden" value="OK" id="list-name-ok" type="button"/>
<input class="btn btn-danger hidden" value="Cancel" id="list-name-cancel" type="button"/>
