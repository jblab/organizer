<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<t:template title="Diary">
    <jsp:attribute name="head">
        <link type="text/css" href="/resources/css/jquery-ui.min.css" rel="stylesheet"/>
        <script src="/resources/js/jquery.min.js"></script>
        <script src="/resources/js/jquery-ui.min.js"></script>
        <script src="/resources/js/tinymce/tinymce.min.js"></script>
    </jsp:attribute>
    <jsp:attribute name="body">
<h2 align='center'> Your diary </h2>

<div id="datepicker"></div>
<form:form action="/diary/add" modelAttribute="note" method="post">
    Запись: <form:textarea class="noteText" path="noteText"/> <form:errors path="noteText"/><br/>
</form:form>
<script src="/resources/js/diaryScript.js"></script>
    </jsp:attribute>
</t:template>