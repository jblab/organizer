<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<t:template title="sdfdsf">
    <jsp:attribute name="head">
        <link href="/resources/css/sportRes.css" rel="stylesheet"/>
         <meta name="description" content="Сайт об HTML и создании сайтов">
    </jsp:attribute>
<jsp:attribute name="body">

    Hello for everyone!
My name is ${name}
    <c:forEach items="${resultsList}" var="result">
        ${result}
        <br>
    </c:forEach> <br>

<form action="/hello/add" method="post">
    <input type="text" class="js-name" name="name" placeholder="Add Value">
    <input type="submit" value="Button" class="addButton">
    <button type="submit" id="add_btn" class="addButton">Add value JS</button>
</form>
<ul>
    <c:forEach items="${helloList}" var="hello">
        <li>${hello.name}</li>
    </c:forEach>
</ul>

<a href="/shop/lists" class="addButton">магазин</a>
<a href="/todo/show" class="addButton">TODO</a>
<a href="/events" class="addButton">Events</a>
<a href="/timetable/show" class="addButton">TimeTable</a>
<a href="/sportResults/sportRes" class="addButton">sportResults</a>
<a href="/diary" class="addButton">Diary</a>
<a href="/friend" class="addButton">Friend</a>



<script type="text/javascript">
    $("#add_btn").on('click', function (e) {
        e.preventDefault();// don't make standard submit.
        // reject event and write your implementation
        var name = $(".js-name").val();
        $.ajax({
            type: "POST",
            url: "/sumAjax",
            data: {"a": 3, "b": 5, "name": name},
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        })
    })
    ;
</script>
</jsp:attribute>
</t:template>

