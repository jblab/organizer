<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="task" id="${result.id}">
    <label class="task-name">${result.taskname}</label><br>
    <c:if test="${not empty result.comment}">
        <p class="task-comment">${result.comment}</p>

        <p class="task-comment-off"></p>
    </c:if>
    <p>
        <c:choose>
            <c:when test="${result.redacted eq 0}">
                <label class="date">Создана: ${result.dateString}</label><br>
            </c:when>
            <c:otherwise>
                <label class="date">Изменена: ${result.dateString}</label><br>
            </c:otherwise>
        </c:choose>
        <c:if test="${not empty result.deadlineString}">
            <label class="deadline">Выполнить до: ${result.deadlineString}</label>
        </c:if>
        <c:if test="${result.failed eq true}">
            <label class="failed"> DEADLINE </label>
        </c:if>
    </p>
    <label class="priority">${result.priority}</label>

    <div class="controls">
        <c:if test="${not (param.status eq 1)}">
            <a href="/todo/changestatus/${result.id}/${param.status - 1}"><i class="fa fa-arrow-left"></i>
            </a>
        </c:if>

        <a href="/todo/remove/${result.id}"> <i class="fa fa-trash-o"></i>
        </a>

        <a href="/todo/edit/${result.id}"><i class="fa fa-pencil"></i>
        </a>

        <c:if test="${not (param.status eq 4)}">
            <a href="/todo/changestatus/${result.id}/${param.status + 1}"><i class="fa fa-arrow-right"></i>
            </a>
        </c:if>
    </div>
</div>