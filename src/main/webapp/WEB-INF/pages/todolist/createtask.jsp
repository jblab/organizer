<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form:form class="add-task" action="/todo/add" method="post" modelAttribute="addform">
    <h1> Добавить задачу </h1>
    <form:errors path="taskname" cssClass="error"/>
    <p><form:input type="text" path="taskname" class="js-taskname"></form:input>
        <form:select path="priority" class="js-priority">
            <option selected value="${defaultTaskPriority.value}">Приоритет</option>
            <c:forEach var="taskPriority" items="${taskPriorities}">
                <form:option value="${taskPriority}">${taskPriority.value}</form:option>
            </c:forEach>
        </form:select></p>
    <p><form:errors path="deadlineDate" cssClass="error"/></p>
    Deadline:
    <form:input type="date" path="deadlineDate" class="js-deadlinedate"/>
    <form:input type="time" path="deadlineTime" class="js-deadlinetime"/>
    <form:input type="hidden" path="status" value="1" class="js-status"/>
    <form:input type="hidden" path="redacted" value="0" class="js-redacted"/>
    <p>
        <form:textarea path="comment" cols="45" rows="5" class="js-comment"></form:textarea></p>
    <button id="create-task-btn" class="button">Добавить</button>
    <input type="reset" value="Очистить" class="button">

</form:form>

