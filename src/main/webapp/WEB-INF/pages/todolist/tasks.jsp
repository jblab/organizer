<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Список задач</title>

    <script src="../resources/js/jquery.min.js"></script>
    <script src="../resources/js/todoscript.js"></script>
    <link href="../resources/css/todostyle.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<div class="tasks-area">
    <div class="tasks">
        <div id="new-task-adding"></div>
        <h1>Cписок задач: </h1>
        <div class="add-button">
            <a href="" id="add-new-task">Добавить ещё</a><br>
            <a href="/todo/clean">Удалить все</a>
        </div>

        <div class="task-row tasks-new">
            <jsp:include page="taskinclusion.jsp">
                <jsp:param name="status" value="1"/>
                <jsp:param name="text" value="Открытые: "/>
            </jsp:include>
        </div>

        <div class="task-row tasks-doing">
            <jsp:include page="taskinclusion.jsp">
                <jsp:param name="status" value="2"/>
                <jsp:param name="text" value="В обработке: "/>
            </jsp:include>
        </div>

        <div class="task-row tasks-check">
            <jsp:include page="taskinclusion.jsp">
                <jsp:param name="status" value="3"/>
                <jsp:param name="text" value="На проверке: "/>
            </jsp:include>
        </div>

        <div class="task-row tasks-done">
            <jsp:include page="taskinclusion.jsp">
                <jsp:param name="status" value="4"/>
                <jsp:param name="text" value="Закрытые: "/>
            </jsp:include>
        </div>
    </div>
</div>

</body>
</html>
