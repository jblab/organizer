<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Редактирование задачи</title>
    <style>
        <%@include file="/WEB-INF/resources/css/todostyle.css" %>
    </style>
</head>
<body>

<form:form class="add-task" action="/todo/update/${taskToEdit.id}" method="post" modelAttribute="taskToEdit">
    <h1> Редактировать задачу </h1>
    <form:errors path="taskname" cssClass="error"/>
    <p><form:input type="text" path="taskname"></form:input>
        <form:select path="priority">
            <c:forEach var="taskPriority" items="${taskPriorities}">
                <form:option value="${taskPriority}">${taskPriority.value}</form:option>
            </c:forEach>
        </form:select></p>
    <p><form:errors path="deadlineDate" cssClass="error"/></p>
    Deadline:
    <form:input type="date" path="deadlineDate"/>
    <form:input type="time" path="deadlineTime"/>
    <form:input type="hidden" path="status" value="${taskToEdit.status}"/>
    <form:input type="hidden" path="redacted" value="1"/>
    <p>
        <form:textarea path="comment" cols="45" rows="5"></form:textarea></p>
    <input type="submit" value="Редактировать" class="button">
    <input type="reset" value="Сбросить" class="button">
</form:form>
<div class="bottom">
    <a href="/todo/show">Вернуться к списку задач</a>
</div>
</body>
</html>
