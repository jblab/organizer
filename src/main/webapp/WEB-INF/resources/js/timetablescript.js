/**
 * Created by Dilya on 24.10.2014.
 */
$(document).ready(function(){
    var weekList = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    var count = 0;
    $('#js-right').on('click', function () {
        $('#js-week').val(weekList[day(++count)]);
        $('#js-day').text(weekList[count]);
    });

    $('#js-left').on('click', function () {
        $('#js-week').val(weekList[day(--count)]);
        $('#js-day').text(weekList[count]);

    });
    $('#js-right1').on('click', function () {
        $('#js-day').text(weekList[day(++count)]);
        $.ajax ({
            type: "POST",
            url: "/timetable/showAjax",
            data: {"weekday": weekList[count]},
            success: function (data) {
                $(".js-render").html(data);

            },
            error: function (data) {
                console.log(data);
            }

        })
    });

    $('#js-left1').on('click', function () {
        $('#js-day').text(weekList[day(--count)]);
        $.ajax ({
            type: "POST",
            url: "/timetable/showAjax",
            data: {"weekday": weekList[count]},
            success: function (data) {
                $(".js-render").html(data);

            },
            error: function (data) {
                console.log(data);
            }

        })
    });

    function day(i) {
        if (i> 6) {
            count = 0;
            return count;
        } else if (i < 0) {
            count = 6;
            return count;
        } else {
            return count;
        }
    }

    $("#add_btn").on('click', function (e) {
        e.preventDefault();
        var time= $('.js-time').val();
    var eventName = $('.js-eventName').val();
    var weekday = $("input[name=weekday]:checked").val();
    $.ajax({
        type: "POST",
        url: "/timetable/addAjax",
        data: {"time": time, "eventName": eventName, "weekday": weekday},
        success: function (data) {
            console.log(data);

        },
        error: function (data) {
            console.log(data);
        }
    })
})})