$(document).ready(function () {
    $('.js-edit').on('click', bindEventToEditButton);
    $('#edit-name-list').on('click', editListName);
    $('.js-del').on('click', deleteGoods);
    $('.js-del-list').on('click',deleteList);

    if(isNaN($('#sum-price').text())){
        $('#sum-price').html("0");
    }
    function checkListIsEmpty() {
        if ($('.js-list').length == 0) {
            $('#empty-list').removeClass("hidden");
        }
    }

    checkListIsEmpty();

    $('#js-add').on('click', function () {
        var obj = $('form').serialize();
        var $listId = $('#list_id').val();
        var $price = $('#price').val();
        var $fullPrice = $('#sum-price').text();
        console.log(obj);
        $.ajax({
            type: "POST",
            url: "/shop/add-ajax",
            data: obj,
            success: function (data) {
                $('tbody').append(data);
                $('.js-edit').on('click', bindEventToEditButton);
                $('#edit-name-list').on('click', editListName);
                $('.js-del').on('click', deleteGoods);
                var searchValue = "/shop/list/" + $listId;
                var href = window.location.pathname;
                if (href != searchValue) {
                    window.location.href = searchValue;
                }
                clearInputs();
                $('#sum-price').html((parseInt($price)+parseInt($fullPrice)));
            },
            error: function (data) {
                //validInput();
                console.log(data);
            }
        });
        return false;
    });


    $('#js-update').on('click', function () {
        var id = $('#goods-id').val();
        var obj = $('form').serialize();
        var $fullPrice = $('#sum-price').text();
        var $priceOld = $('#priceTable'+id).ignore('label').text();
        $.ajax({
            type: "POST",
            url: "/shop/edit/" + id,
            data: obj,
            success: function (data) {
                $('#js-tr-' + id).html(data);
                $('#js-add').removeClass("hidden");
                $('#js-update').addClass("hidden");
                $('#js-cancel').addClass("hidden");

                $('.js-edit').on('click', bindEventToEditButton);
                $('#edit-name-list').on('click', editListName);
                $('#js-del').on('click', deleteGoods);
                var $priceNew = $('#price').val();
                clearInputs();
                $('#sum-price').html((parseInt($fullPrice)-(parseInt($priceOld)))+parseInt($priceNew));
            },
            error: function (data) {
                //validInput();
                console.log(data);
            }
        });
        return false;
    });


    /**
     * functions
     */
    $.fn.ignore = function (sel) {
        return this.clone().find(sel).remove().end();
    };

    function editListName() {
        $('.list-name').addClass("hidden");
        $('#edit-name-list').addClass("hidden");
        $('#list-name-input').removeClass("hidden");
        $('#list-name-input').val(($('.list-name h1').text()));
        $('#list-name-ok').removeClass("hidden");
        $('#list-name-cancel').removeClass("hidden");
        $('#list-name-cancel').on('click', function () {
            $('.list-name').removeClass("hidden");
            $('#edit-name-list').removeClass("hidden");
            $('#list-name-input').addClass("hidden").val('');
            $('#list-name-ok').addClass("hidden");
        });
        $('#list-name-ok').on('click', function () {
            var id = $('#list_id').val();
            var name = $('#list-name-input').val();
            $.ajax({
                type: "POST",
                url: "/shop/editNameList/" + id,
                data: {
                    "name": name,
                    "id": id
                },
                success: function (data) {
                    $('.list-name').removeClass("hidden");
                    $('#edit-name-list').removeClass("hidden");
                    $('#list-name-input').addClass("hidden").val('');
                    $('#list-name-ok').addClass("hidden");
                    $('#list-name-cancel').addClass("hidden");
                    $('.list-name h1').html(name);
                },
                error: function (data) {
                    console.log(data);
                }
            })
        })


    }

    function clearInputs() {
        $('#category').val('');
        $('#name').val('');
        $('#count-goods').val('');
        $('#typeCount').val('');
        $('#price').val('');
        $('#comment').val('');
    }

    $('.nonString').keypress(function (b) {
        var C = /[0-9\x25\x27\x24\x23]/;
        var a = b.which;
        var c = String.fromCharCode(a);
        return !!(a == 0 || a == 8 || a == 9 || a == 13 || c.match(C));
    });

    /**
     * Потом
     */
        //function validInput() {
        //    if ($('#category').val() == '') {
        //        $('#category').wrap('<div class="category-js-valid form-group has-warning has-feedback"></div>');
        //    }
        //    if ($('#name').val() == '') {
        //        $('#name').wrap('<div class="name-js-valid form-group has-warning has-feedback"></div>');
        //    }
        //    if ($('#count-goods').val() == '') {
        //        $('#count-goods').wrap('<div class="count-goods-js-valid form-group has-warning has-feedback"></div>');
        //    }
        //    if ($('#price').val() == '') {
        //        $('#price').wrap('<div class="price-js-valid form-group has-warning has-feedback"></div>');
        //    }
        //}

    function bindEventToEditButton() {
        var id = this.id;
        $('#goods-id').val(id);
        $('#category').val($('#categoryTable' + id).text());
        $('#name').val($('#nameTable' + id).text());
        $('#count-goods').val($('#countGoodsTable' + id).ignore('label').text());
        $('#typeCount').val($('#typeCountTable' + id).val());
        $('#price').val($('#priceTable' + id).ignore('label').text());
        $('#comment').val($('#commentTable' + id).text());
        $('#id-edit').val(id);
        $('#js-add').addClass("hidden");
        $('#js-update').removeClass("hidden");
        $('#js-cancel').removeClass("hidden");
        $('#js-cancel').click(function () {
            $('#js-add').removeClass("hidden");
            $('#js-update').addClass("hidden");
            $('#js-cancel').addClass("hidden");
            clearInputs();
        });
    }

    function deleteGoods() {
        var id = $(this, '.js-del').val();
        var $fullPrice = $('#sum-price').text();
        var $price = $('#priceTable'+id).ignore('label').text();
        $.ajax({
            type: "POST",
            url: "/shop/remove/goods" + id,
            data: id,
            success: function () {
                $('#js-tr-'+id).remove();
                $('#sum-price').html(parseInt($fullPrice)-(parseInt($price)));
            },
            error: function () {
                console.log("error");
            }
        })
    }
    function deleteList() {
        var id = $(this, '.js-del-list').val();

        $.ajax({
            type: "POST",
            url: "/shop/remove/list" + id,
            data: id,
            success: function () {
                $('#list-' + id).remove();
                checkListIsEmpty();
            },
            error: function () {
                console.log("error");
            }
        });
    }
});

