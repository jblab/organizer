/**
 * Created by Rigen on 28.10.14.
 */
$(document).ready(function () {
    function dateFunction() {
        var date = $("#datepicker").datepicker("getDate");
        var year = date.getFullYear();
        var day = ('0' + date.getDate()).slice(-2);
        var month = ('0' + (date.getMonth() + 1)).slice(-2);
        return (year + "-" + month + "-" + day);
    };
    tinymce.init({
        language: "ru",
        selector: ".noteText",

        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | delete",
        toolbar2: "bullist numlist outdent indent | link image media | forecolor backcolor emoticons",
        menubar: false,
        statusbar: false,
        width: 500,
        setup: function (editor) {
            editor.addButton('delete', {
                text: 'Delete',
                icon: false,
                onclick: function () {
                    editor.setContent("");
                    var noteText = editor.getContent();
                    saveDairy(dateFunction(), noteText);
                }
            });
            editor.on('blur', function () {
                var noteText = editor.getContent();
                saveDairy(dateFunction(), noteText);
            });
            editor.on('init', function () {
                getDairyByDate(dateFunction());
            })
        }
    });

    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function () {
            getDairyByDate(dateFunction());
        }
    });

    function saveDairy(date, text) {
        $.ajax({
            type: "POST",
            url: "/diary/addOrEdit",
            dataType: "text",
            data: {"date": date, "noteText": text},
            success: function (data) {
            },
            error: function (data) {
                console.log(data);
            }
        })
    }

    function getDairyByDate(date) {
        $.ajax({
            type: "GET",
            url: "/diary/findByDate",
            data: {"date": date},
            success: function (data) {
                if (data == null) {
                    tinyMCE.activeEditor.setContent("");
                } else {
                    tinyMCE.activeEditor.setContent(data);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
})
