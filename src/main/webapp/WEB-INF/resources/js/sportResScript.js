
$(document).ready(function() {

    $("#add_btn").on('click', function (e) {
        e.preventDefault();// don't make standard submit.
        // reject event and write your implementation
        var $name = $(".js-kindOfSportName").val();
        var $enumName = $(".js-sportsKindEnum").val();
        var $time = $(".js-time").val();
        var $metric = $(".js-metric").val();

        $.ajax({
            type: "POST",
            url: "/sportResults/addAjax",
            data: {
                "name": $name,
                "sportsKindEnum": $enumName,
                "time": $time,
                "metric": $metric
            },
            success: function (data) {
                $('#addSportResJspInclude').append(data);
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $("#edit_btn").on('click', function (e) {
        e.preventDefault();
        var $name = $(".js-kindOfSportName").val();
        var $enumName = $(".js-sportsKindEnum").val();
        var $time = $(".js-time").val();
        var $metric = $(".js-metric").val();
        var $id = $(".js-id").val();

        $.ajax({
            type: "POST",
            url: "/sportResults/editAjax",
            data: {
                "name": $name,
                "sportsKindEnum": $enumName,
                "time": $time,
                "metric": $metric,
                "id": $id
            },
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    //To open page with the list of sport results at the current date (DEFAULT); // CURRENT_STAM;
    function currentDateFunction() {
        var date = new Date();
        var year = date.getFullYear();
        var day = ('0' + date.getDate()).slice(-2);
        var month = ('0' + (date.getMonth() + 1)).slice(-2);
        return (year + "-" + month + "-" + day);
    }

    $(function () {

        $.ajax({
            type: "GET",
            url: "/sportResults/findByDateAjax",
            data: {
                "date": currentDateFunction()
            },
            success: function (data) {
                $("#addSpecSportResListInclude").html(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    })

    function dateFunction() {
        var date = $("#datepicker").datepicker("getDate");
        var year = date.getFullYear();
        var day = ('0' + date.getDate()).slice(-2);
        var month = ('0' + (date.getMonth() + 1)).slice(-2);

        return (year + "-" + month + "-" + day);
    }

    $(function () {
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            onSelect: function () {
                $.ajax({
                    type: "GET",
                    url: "/sportResults/findByDateAjax",
                    data: {
                        "date": dateFunction()
                    },
                    success: function (data) {
                        $("#addSpecSportResListInclude").html(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        });
    });

//****************************************************************

    $(function () {
        $("#clear_list").on('click', function (e) {
            e.preventDefault();
            var date = dateFunction()  //$(".js-dateStr").val();

            $.ajax({
                type: "POST",
                url: "/sportResults/clearSportResAjax",
                data: {
                    "date": date
                },
                success: function (data) {
                    $("#addSpecSportResListInclude").html(data);
                    //success: Results have been deleted. $("#successDeletion").html(data) / .append(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    $(function () {
        $(".js-delete_result").live('click', function (e) {   //'div-content',
            e.preventDefault();
            var kindOfsport_id = $(this).val();   //$(".js-id").val();

            $.ajax({
                type: "GET",
                url: "/sportResults/deleteSportResAjax",
                data: {
                    "id": kindOfsport_id
                },
                success: function (data) {
                    $("#addSpecSportResListInclude").html(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

})

//Метод объекта event preventDefault позволяет предотвратить выполнение
// стандартного действия, например такого как переход на сайт после щелчка
// по ссылке или отправка формы после щелчка на кнопку отправления
// (то есть input type=submit).