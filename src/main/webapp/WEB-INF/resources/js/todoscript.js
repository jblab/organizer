$(document).ready(function () {
    $('#add-new-task').on("click", function () {
        $.ajax({
            url: "/todo/create",
            dataType: "html",
            success: function (data) {
                $('#new-task-adding').html(data).show("slow");
                $("#create-task-btn").on("click", function (e) {
                    e.preventDefault();
                    formProcess();
                    return false;
                });
            }
        });
        return false;
    });

    $(".tasks-new .failed, .tasks-doing .failed").parents(".task").css("background", "#ffd0bf");
});

$(function () {
    $(document).click(function (event) {
        if ($(event.target).closest("#new-task-adding").length) return;
        $("#new-task-adding").hide("slow");
        event.stopPropagation();
    });
});

$(function () {
    $(".task")
        .click(function () {
            taskClick(this);
        })
        .mouseenter(function () {
            taskMouseEnter(this);
        })
        .mouseleave(function () {
            taskMouseLeave(this);
        });
});


function taskClick(task) {
    var display = $(task).find(".task-comment").css("display");
    if (display == "none") {
        $(task).find(".task-comment").css("display", "inherit");
        $(task).find(".task-comment").fadeIn(1);
        $(task).find(".task-comment-off").text("");
    } else {
        $(task).find(".task-comment").fadeOut(1);
        $(task).find(".task-comment-off").text("...");
    }
}

function taskMouseEnter(task) {
    $(task).find(".date, .deadline, .priority").fadeTo("fast", 1);
    $(task).find(".controls").fadeTo("fast", 1);
    return false;
}

function taskMouseLeave(task) {
    $(task).find(".date, .deadline, .priority").fadeTo("fast", 0.75);
    $(task).find(".controls").fadeTo("fast", 0);
    return false;
}


function formProcess() {
    var $taskname = $(".js-taskname").val();
    var $priority = $(".js-priority").val();
    var $deadlineDate = $(".js-deadlinedate").val();
    var $deadlineTime = $(".js-deadlineTime").val();
    var $status = $(".js-status").val();
    var $redacted = $(".js-redacted").val();
    var $comment = $(".js-comment").val();

    $.ajax({
        type: "POST",
        url: "/todo/addJs",
        data: {
            'taskname': $taskname,
            'priority': $priority,
            'deadlineDate': $deadlineDate,
            'deadlineTime': $deadlineTime,
            'status': $status,
            'redacted': $redacted,
            'comment': $comment
        },
        success: function (data) {
            if ($(data).find(".error").attr("class")) {
                $('#new-task-adding').html(data);
                $("#create-task-btn").on("click", function (e) {
                    data = null;
                    e.preventDefault();
                    formProcess();
                    return false;
                });
            } else {
                $('#new-task-adding').fadeOut("medium", function () {
                    $(this).html("");
                });
                $(".tasks-new").append(data).find(".task")
                    .mouseenter(function () {
                        taskMouseEnter(this);
                    })
                    .mouseleave(function () {
                        taskMouseLeave(this);
                    })
                    .click(function () {
                        taskClick(this);
                    });
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}